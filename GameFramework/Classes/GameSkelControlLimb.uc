/*******************************************************************************
 * GameSkelControlLimb generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class GameSkelControlLimb extends SkelControlBase
    native(Anim)
    hidecategories(Object,Object);

var(Effector) Vector EffectorLocation;
var(Effector) Rotator EffectorRotation;
var(Effector) Engine.SkelControlBase.EBoneControlSpace EffectorSpace;
var(Joint) Engine.SkelControlBase.EBoneControlSpace JointTargetLocationSpace;
var(Limb) Core.Object.EAxis BoneAxis;
var(Limb) Core.Object.EAxis JointAxis;
var(Effector) name EffectorSpaceBoneName;
var(Effector) bool bMaintainEffectorRelRot;
var(Effector) bool bTakeRotationFromEffectorSpace;
var(Joint) bool bConstrainJointAngles;
var(Limb) bool bInvertBoneAxis;
var(Limb) bool bInvertJointAxis;
var(Limb) bool bRotateJoint;
var(Stretching) bool bAllowStretching;
var BoneAtom CachedUnmodifiedEndBoneAtom;
var(Joint) Vector JointTargetLocation;
var(Joint) name JointTargetSpaceBoneName;
var(Joint) float MaxJointAngle;
var(Stretching) Vector2D StretchLimits;
var(Stretching) name StretchRollBoneName;
var export editinline SkeletalMeshComponent EffectorSpaceOtherComponent;

defaultproperties
{
    BoneAxis=EAxis.AXIS_X
    JointAxis=EAxis.AXIS_Y
    StretchLimits=(X=0.710,Y=1.20)
    bIgnoreWhenNotRendered=true
}