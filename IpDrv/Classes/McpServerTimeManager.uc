/*******************************************************************************
 * McpServerTimeManager generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class McpServerTimeManager extends McpServerTimeBase
    config(Engine);

var config string TimeStampUrl;
var string LastTimeStamp;
var HttpRequestInterface HTTPRequestServerTime;

function QueryServerTime()
{
    local string URL, ErrorStr;
    local bool bPending;

    // End:0x156
    if(HTTPRequestServerTime == none)
    {
        HTTPRequestServerTime = class'HttpFactory'.static.CreateRequest();
        // End:0x153
        if(HTTPRequestServerTime != none)
        {
            URL = ((GetBaseURL()) $ TimeStampUrl) $ (GetAppAccessURL());
            HTTPRequestServerTime.SetURL(URL);
            HTTPRequestServerTime.SetVerb("GET");
            HTTPRequestServerTime.__OnProcessRequestComplete__Delegate = OnQueryServerTimeHTTPRequestComplete;
            // End:0x11F
            if(HTTPRequestServerTime.ProcessRequest())
            {
                bPending = true;
            }
            // End:0x153
            else
            {
                ErrorStr = "failed to start request, Url=" $ URL;
            }
        }
    }
    // End:0x187
    else
    {
        ErrorStr = "last request is still being processed";
    }
    // End:0x1B5
    if(!bPending)
    {
        OnQueryServerTimeComplete(false, "", ErrorStr);
    }
    //return;    
}

private final function OnQueryServerTimeHTTPRequestComplete(HttpRequestInterface Request, HttpResponseInterface Response, bool bWasSuccessful)
{
    local string TimeStr, ResponseStr, ErrorStr;
    local int Idx;
    local bool bResult;

    HTTPRequestServerTime = none;
    // End:0x181
    if(bWasSuccessful && Response != none)
    {
        // End:0x12A
        if(Response.GetResponseCode() == 200)
        {
            ResponseStr = Response.GetContentAsString();
            Idx = InStr(ResponseStr, "Timestamp=");
            // End:0x127
            if(Idx != -1)
            {
                TimeStr = Mid(ResponseStr, Idx);
                Idx = InStr(ResponseStr, "=");
                TimeStr = Mid(TimeStr, Idx);
                LastTimeStamp = TimeStr;
                bResult = true;
            }
        }
        // End:0x17E
        else
        {
            ErrorStr = "invalid server response code, status=" $ string(Response.GetResponseCode());
        }
    }
    // End:0x198
    else
    {
        ErrorStr = "no response";
    }
    OnQueryServerTimeComplete(bResult, TimeStr, ErrorStr);
    //return;    
}

function string GetLastServerTime()
{
    return LastTimeStamp;
    //return ReturnValue;    
}

defaultproperties
{
    TimeStampUrl="/timestamp"
}