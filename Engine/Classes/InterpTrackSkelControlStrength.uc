/*******************************************************************************
 * InterpTrackSkelControlStrength generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class InterpTrackSkelControlStrength extends InterpTrackFloatBase
    native(Interpolation)
    collapsecategories
    hidecategories(Object);

var() name SkelControlName;

defaultproperties
{
    TrackInstClass=class'InterpTrackInstSkelControlStrength'
    TrackTitle="SkelControl Strength"
    bIsAnimControlTrack=true
}