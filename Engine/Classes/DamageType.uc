/*******************************************************************************
 * DamageType generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class DamageType extends Object
    abstract
    native;

var() bool bArmorStops;
var bool bCausedByWorld;
var bool bExtraMomentumZ;
var() bool bCausesFracture;
var(RigidBody) bool bRadialDamageVelChange;
var bool bSuppressDeathMessage;
var bool bAwardNoPoints;
var(RigidBody) float KDamageImpulse;
var(RigidBody) float KDeathVel;
var(RigidBody) float KDeathUpKick;
var(RigidBody) float RadialDamageImpulse;
var float VehicleDamageScaling;
var float VehicleMomentumScaling;
var ForceFeedbackWaveform DamagedFFWaveform;
var ForceFeedbackWaveform KilledFFWaveform;
var float FracturedMeshDamage;

static function float VehicleDamageScalingFor(Pawn P)
{
    return default.VehicleDamageScaling;
    //return ReturnValue;    
}

defaultproperties
{
    bArmorStops=true
    bExtraMomentumZ=true
    KDamageImpulse=800.0
    VehicleDamageScaling=1.0
    VehicleMomentumScaling=1.0
    FracturedMeshDamage=1.0
}