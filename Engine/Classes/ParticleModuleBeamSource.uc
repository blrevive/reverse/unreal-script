/*******************************************************************************
 * ParticleModuleBeamSource generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class ParticleModuleBeamSource extends ParticleModuleBeamBase
    native(Particle)
    editinlinenew
    hidecategories(Object,Object,Object);

var(Source) ParticleModuleBeamBase.Beam2SourceTargetMethod SourceMethod;
var(Source) ParticleModuleBeamBase.Beam2SourceTargetTangentMethod SourceTangentMethod;
var(Source) name SourceName;
var(Source) bool bSourceAbsolute;
var(Source) bool bLockSource;
var(Source) bool bLockSourceTangent;
var(Source) bool bLockSourceStength;
var(Source) RawDistributionVector Source;
var(Source) RawDistributionVector SourceTangent;
var(Source) RawDistributionFloat SourceStrength;

defaultproperties
{
    Source=Distribution=DistributionVectorConstant'Default__ParticleModuleBeamSource.DistributionSource',Type=0,Op=1,LookupTableNumElements=1,LookupTableChunkSize=3,LookupTable=/* Array type was not detected. */,
/* Exception thrown while deserializing Source
System.ArgumentOutOfRangeException: Der Index lag au?erhalb des Bereichs. Er darf nicht negativ und kleiner als die Sammlung sein.
Parametername: index
   bei System.ThrowHelper.ThrowArgumentOutOfRangeException(ExceptionArgument argument, ExceptionResource resource)
   bei UELib.UName.Deserialize(IUnrealStream stream)
   bei UELib.UObjectStream.ReadNameReference()
   bei UELib.Core.UDefaultProperty.Deserialize()
   bei UELib.Core.UDefaultProperty.DeserializeDefaultPropertyValue(PropertyType type, DeserializeFlags& deserializeFlags) */
    SourceTangent=Distribution=DistributionVectorConstant'Default__ParticleModuleBeamSource.DistributionSourceTangent',Type=0,Op=1,LookupTableNumElements=1,LookupTableChunkSize=3,LookupTable=/* Array type was not detected. */,
/* Exception thrown while deserializing SourceTangent
System.ArgumentOutOfRangeException: Der Index lag au?erhalb des Bereichs. Er darf nicht negativ und kleiner als die Sammlung sein.
Parametername: index
   bei System.ThrowHelper.ThrowArgumentOutOfRangeException(ExceptionArgument argument, ExceptionResource resource)
   bei UELib.UnrealStreamImplementations.ReadName(IUnrealStream stream)
   bei UELib.Core.UDefaultProperty.Deserialize()
   bei UELib.Core.UDefaultProperty.DeserializeDefaultPropertyValue(PropertyType type, DeserializeFlags& deserializeFlags) */
    SourceStrength=Distribution=DistributionFloatConstant'Default__ParticleModuleBeamSource.DistributionSourceStrength',Type=0,Op=1,LookupTableNumElements=1,LookupTableChunkSize=1,LookupTable=/* Array type was not detected. */,
/* Exception thrown while deserializing SourceStrength
System.ArgumentOutOfRangeException: Der Index lag au?erhalb des Bereichs. Er darf nicht negativ und kleiner als die Sammlung sein.
Parametername: index
   bei System.ThrowHelper.ThrowArgumentOutOfRangeException(ExceptionArgument argument, ExceptionResource resource)
   bei UELib.UName.Deserialize(IUnrealStream stream)
   bei UELib.UObjectStream.ReadNameReference()
   bei UELib.Core.UDefaultProperty.Deserialize()
   bei UELib.Core.UDefaultProperty.DeserializeDefaultPropertyValue(PropertyType type, DeserializeFlags& deserializeFlags) */
}