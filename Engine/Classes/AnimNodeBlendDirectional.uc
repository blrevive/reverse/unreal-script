/*******************************************************************************
 * AnimNodeBlendDirectional generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class AnimNodeBlendDirectional extends AnimNodeBlendBase
    native(Anim)
    hidecategories(Object,Object,Object,Object);

var() float DirDegreesPerSecond;
var float DirAngle;
var() int SingleAnimAtOrAboveLOD;
var Rotator RotationOffset;
var() bool bUseAcceleration;

defaultproperties
{
    DirDegreesPerSecond=360.0
    SingleAnimAtOrAboveLOD=1000
    Children(0)=(Name=Forward,Anim=none,Weight=1.0,BlendWeight=0.0,bMirrorSkeleton=false,bIsAdditive=false,DrawY=0)
    Children(1)=(Name=Backward,Anim=none,Weight=0.0,BlendWeight=0.0,bMirrorSkeleton=false,bIsAdditive=false,DrawY=0)
    Children(2)=(Name=Left,Anim=none,Weight=0.0,BlendWeight=0.0,bMirrorSkeleton=false,bIsAdditive=false,DrawY=0)
    Children(3)=(Name=Right,Anim=none,Weight=0.0,BlendWeight=0.0,bMirrorSkeleton=false,bIsAdditive=false,DrawY=0)
    bFixNumChildren=true
}