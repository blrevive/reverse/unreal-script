/*******************************************************************************
 * AutoLadder generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class AutoLadder extends Ladder
    native
    notplaceable
    hidecategories(Navigation,Lighting,LightColor,Force);

defaultproperties
{
    begin object name=CollisionCylinder class=CylinderComponent
        ReplacementPrimitive=none
    object end
    // Reference: CylinderComponent'Default__AutoLadder.CollisionCylinder'
    CylinderComponent=CollisionCylinder
    Components(0)=none
    Components(1)=none
    Components(2)=none
    begin object name=CollisionCylinder class=CylinderComponent
        ReplacementPrimitive=none
    object end
    // Reference: CylinderComponent'Default__AutoLadder.CollisionCylinder'
    Components(3)=CollisionCylinder
    Components(4)=none
    bCollideWhenPlacing=false
    begin object name=CollisionCylinder class=CylinderComponent
        ReplacementPrimitive=none
    object end
    // Reference: CylinderComponent'Default__AutoLadder.CollisionCylinder'
    CollisionComponent=CollisionCylinder
}