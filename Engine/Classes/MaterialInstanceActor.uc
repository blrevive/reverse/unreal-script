/*******************************************************************************
 * MaterialInstanceActor generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class MaterialInstanceActor extends Actor
    native
    placeable
    hidecategories(Navigation,Movement,Advanced,Collision,Display,Actor,Attachment);

var() MaterialInstanceConstant MatInst;

defaultproperties
{
    Components(0)=none
    TickGroup=ETickingGroup.TG_DuringAsyncWork
    bNoDelete=true
}