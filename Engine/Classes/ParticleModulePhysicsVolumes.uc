/*******************************************************************************
 * ParticleModulePhysicsVolumes generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class ParticleModulePhysicsVolumes extends ParticleModuleWorldForcesBase
    native(Particle)
    editinlinenew
    hidecategories(Object,Object,Object);

enum EParticleLevelInfluenceType
{
    LIT_Never,
    LIT_OutsidePhysicsVolumes,
    LIT_Always,
    LIT_MAX
};

var() RawDistributionFloat GlobalInfluence;
var() ParticleModulePhysicsVolumes.EParticleLevelInfluenceType LevelInfluenceType;

defaultproperties
{
    GlobalInfluence=Distribution=DistributionFloatConstant'Default__ParticleModulePhysicsVolumes.DistributionInfluence',Type=0,Op=1,LookupTableNumElements=1,LookupTableChunkSize=1,LookupTable=/* Array type was not detected. */,
/* Exception thrown while deserializing GlobalInfluence
System.ArgumentException: Der angeforderte Wert "*" konnte nicht gefunden werden.
   bei System.Enum.TryParseEnum(Type enumType, String value, Boolean ignoreCase, EnumResult& parseResult)
   bei System.Enum.Parse(Type enumType, String value, Boolean ignoreCase)
   bei UELib.Core.UDefaultProperty.Deserialize()
   bei UELib.Core.UDefaultProperty.DeserializeDefaultPropertyValue(PropertyType type, DeserializeFlags& deserializeFlags) */
    bUpdateModule=true
}