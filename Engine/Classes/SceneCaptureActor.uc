/*******************************************************************************
 * SceneCaptureActor generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class SceneCaptureActor extends Actor
    abstract
    native
    notplaceable
    hidecategories(Navigation);

var() const export editinline SceneCaptureComponent SceneCapture;

simulated function OnToggle(SeqAct_Toggle Action)
{
    local bool bEnable;

    // End:0x11
    if(SceneCapture == none)
    {
        return;
    }
    // End:0x57
    if(Action.InputLinks[0].bHasImpulse)
    {
        bEnable = true;
    }
    // End:0x101
    else
    {
        // End:0x9D
        if(Action.InputLinks[1].bHasImpulse)
        {
            bEnable = false;
        }
        // End:0x101
        else
        {
            // End:0x101
            if(Action.InputLinks[2].bHasImpulse)
            {
                bEnable = !SceneCapture.bEnabled;
            }
        }
    }
    SceneCapture.SetEnabled(bEnable);
    //return;    
}

defaultproperties
{
    Components(0)=none
    RemoteRole=ENetRole.ROLE_SimulatedProxy
    bNoDelete=true
}