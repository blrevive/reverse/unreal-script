/*******************************************************************************
 * InterpData generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class InterpData extends SequenceVariable
    native(Sequence)
    hidecategories(Object);

struct native AnimSetBakeAndPruneStatus
{
    var() editconst string AnimSetName;
    var() editconst bool bReferencedButUnused;
    var() bool bSkipBakeAndPrune;
    var() bool bSkipCooking;

    structdefaultproperties
    {
        AnimSetName=""
        bReferencedButUnused=false
        bSkipBakeAndPrune=false
        bSkipCooking=false
    }
};

var float InterpLength;
var float PathBuildTime;
var export array<export InterpGroup> InterpGroups;
var export InterpCurveEdSetup CurveEdSetup;
var editoronly array<editoronly InterpFilter> InterpFilters;
var editoronly InterpFilter SelectedFilter;
var editoronly transient array<editoronly InterpFilter> DefaultFilters;
var float EdSectionStart;
var float EdSectionEnd;
var() bool bShouldBakeAndPrune;
var() editfixedsize array<AnimSetBakeAndPruneStatus> BakeAndPruneStatus;
var transient InterpGroupDirector CachedDirectorGroup;

defaultproperties
{
    InterpLength=5.0
    EdSectionEnd=1.0
    ObjName="Matinee Data"
}