/*******************************************************************************
 * RB_CylindricalForceActor generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class RB_CylindricalForceActor extends RigidBodyBase
    native(ForceField)
    placeable
    hidecategories(Navigation);

var() export editinline DrawCylinderComponent RenderComponent;
var() interp float RadialStrength;
var() interp float RotationalStrength;
var() interp float LiftStrength;
var() interp float LiftFalloffHeight;
var() interp float EscapeVelocity;
var() interp float ForceRadius;
var() interp float ForceTopRadius;
var() interp float ForceHeight;
var() interp float HeightOffset;
var() bool bForceActive;
var() bool bForceApplyToCloth;
var() bool bForceApplyToFluid;
var() bool bForceApplyToRigidBodies;
var() bool bForceApplyToProjectiles;
var() const RBCollisionChannelContainer CollideWithChannels;

replication
{
    // Pos:0x000
    if(bNetDirty)
        bForceActive
}

simulated function OnToggle(SeqAct_Toggle inAction)
{
    // End:0x46
    if(inAction.InputLinks[0].bHasImpulse)
    {
        bForceActive = true;
    }
    // End:0xDB
    else
    {
        // End:0x8C
        if(inAction.InputLinks[1].bHasImpulse)
        {
            bForceActive = false;
        }
        // End:0xDB
        else
        {
            // End:0xDB
            if(inAction.InputLinks[2].bHasImpulse)
            {
                bForceActive = !bForceActive;
            }
        }
    }
    SetForcedInitialReplicatedProperty(boolproperty'bForceActive', bForceActive == default.bForceActive);
    //return;    
}

defaultproperties
{
    begin object name=DrawCylinder0 class=DrawCylinderComponent
        CylinderRadius=200.0
        CylinderTopRadius=200.0
        CylinderHeight=200.0
        bDrawOnlyIfSelected=true
        ReplacementPrimitive=none
    object end
    // Reference: DrawCylinderComponent'Default__RB_CylindricalForceActor.DrawCylinder0'
    RenderComponent=DrawCylinder0
    EscapeVelocity=10000.0
    ForceRadius=200.0
    ForceTopRadius=200.0
    ForceHeight=200.0
    bForceApplyToCloth=true
    bForceApplyToFluid=true
    bForceApplyToRigidBodies=true
    CollideWithChannels=(Default=true,Nothing=false,Pawn=true,Vehicle=true,Water=true,GameplayPhysics=true,EffectPhysics=true,Untitled1=true,Untitled2=true,Untitled3=true,Untitled4=false,Cloth=true,FluidDrain=true,SoftBody=false,FracturedMeshPart=false,BlockingVolume=false,DeadPawn=false,Clothing=false,ClothingCollision=false)
    begin object name=DrawCylinder0 class=DrawCylinderComponent
        CylinderRadius=200.0
        CylinderTopRadius=200.0
        CylinderHeight=200.0
        bDrawOnlyIfSelected=true
        ReplacementPrimitive=none
    object end
    // Reference: DrawCylinderComponent'Default__RB_CylindricalForceActor.DrawCylinder0'
    Components(0)=DrawCylinder0
    Components(1)=none
    RemoteRole=ENetRole.ROLE_SimulatedProxy
    bNoDelete=true
    bAlwaysRelevant=true
    bOnlyDirtyReplication=true
    NetUpdateFrequency=0.10
}