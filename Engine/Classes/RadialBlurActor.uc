/*******************************************************************************
 * RadialBlurActor generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class RadialBlurActor extends Actor
    placeable
    hidecategories(Navigation);

var() export editinline RadialBlurComponent RadialBlur;

defaultproperties
{
    RadialBlur=RadialBlurComponent'Default__RadialBlurActor.RadialBlurComp'
    Components(0)=RadialBlurComponent'Default__RadialBlurActor.RadialBlurComp'
    Components(1)=none
}