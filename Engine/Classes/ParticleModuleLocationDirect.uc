/*******************************************************************************
 * ParticleModuleLocationDirect generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class ParticleModuleLocationDirect extends ParticleModuleLocationBase
    native(Particle)
    editinlinenew
    hidecategories(Object,Object,Object);

var(Location) RawDistributionVector Location;
var(Location) RawDistributionVector LocationOffset;
var(Location) RawDistributionVector ScaleFactor;
var(Location) RawDistributionVector Direction;

defaultproperties
{
    Location=Distribution=DistributionVectorUniform'Default__ParticleModuleLocationDirect.DistributionLocation',Type=0,Op=1,LookupTableNumElements=1,LookupTableChunkSize=3,LookupTable=/* Array type was not detected. */,
/* Exception thrown while deserializing Location
System.ArgumentException: Der angeforderte Wert "*" konnte nicht gefunden werden.
   bei System.Enum.TryParseEnum(Type enumType, String value, Boolean ignoreCase, EnumResult& parseResult)
   bei System.Enum.Parse(Type enumType, String value, Boolean ignoreCase)
   bei UELib.Core.UDefaultProperty.Deserialize()
   bei UELib.Core.UDefaultProperty.DeserializeDefaultPropertyValue(PropertyType type, DeserializeFlags& deserializeFlags) */
    LocationOffset=Distribution=DistributionVectorConstant'Default__ParticleModuleLocationDirect.DistributionLocationOffset',Type=0,Op=1,LookupTableNumElements=1,LookupTableChunkSize=3,LookupTable=/* Array type was not detected. */,
/* Exception thrown while deserializing LocationOffset
System.ArgumentException: Der angeforderte Wert "*" konnte nicht gefunden werden.
   bei System.Enum.TryParseEnum(Type enumType, String value, Boolean ignoreCase, EnumResult& parseResult)
   bei System.Enum.Parse(Type enumType, String value, Boolean ignoreCase)
   bei UELib.Core.UDefaultProperty.Deserialize()
   bei UELib.Core.UDefaultProperty.DeserializeDefaultPropertyValue(PropertyType type, DeserializeFlags& deserializeFlags) */
    ScaleFactor=Distribution=DistributionVectorConstant'Default__ParticleModuleLocationDirect.DistributionScaleFactor',Type=0,Op=1,LookupTableNumElements=1,LookupTableChunkSize=3,LookupTable=/* Array type was not detected. */,
/* Exception thrown while deserializing ScaleFactor
System.ArgumentOutOfRangeException: Der Index lag au?erhalb des Bereichs. Er darf nicht negativ und kleiner als die Sammlung sein.
Parametername: index
   bei System.ThrowHelper.ThrowArgumentOutOfRangeException(ExceptionArgument argument, ExceptionResource resource)
   bei UELib.UName.Deserialize(IUnrealStream stream)
   bei UELib.UObjectStream.ReadNameReference()
   bei UELib.Core.UDefaultProperty.Deserialize()
   bei UELib.Core.UDefaultProperty.DeserializeDefaultPropertyValue(PropertyType type, DeserializeFlags& deserializeFlags) */
    Direction=Distribution=DistributionVectorUniform'Default__ParticleModuleLocationDirect.DistributionDirection',Type=0,Op=1,LookupTableNumElements=1,LookupTableChunkSize=3,LookupTable=/* Array type was not detected. */,
/* Exception thrown while deserializing Direction
System.ArgumentException: Der angeforderte Wert "*" konnte nicht gefunden werden.
   bei System.Enum.TryParseEnum(Type enumType, String value, Boolean ignoreCase, EnumResult& parseResult)
   bei System.Enum.Parse(Type enumType, String value, Boolean ignoreCase)
   bei UELib.Core.UDefaultProperty.Deserialize()
   bei UELib.Core.UDefaultProperty.DeserializeDefaultPropertyValue(PropertyType type, DeserializeFlags& deserializeFlags) */
    bSpawnModule=true
    bUpdateModule=true
}