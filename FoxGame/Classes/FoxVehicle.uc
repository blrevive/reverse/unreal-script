/*******************************************************************************
 * FoxVehicle generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxVehicle extends FoxPawn
    abstract
    native(Vehicle)
    nativereplication
    config(Pawn)
    hidecategories(Navigation);

struct native CrushedPawnInfo
{
    var FoxPawn CrushedPawn;
    var float CrushedTime;

    structdefaultproperties
    {
        CrushedPawn=none
        CrushedTime=0.0
    }
};

var protected repnotify transient FoxPawn Driver;
var protected repnotify transient bool bDriving;
var protected const bool bDriverIsVisible;
var protected const bool bAttachDriver;
var protected const bool bDoExtraNetRelevancyTraces;
var protected transient float Steering;
var protected transient float Throttle;
var protected transient float Rise;
var protected transient float ExitRadius;
var protected const array<Vector> ExitPositions;
var protected const Vector ExitOffset;
var protected const Vector TargetLocationAdjustment;
var protected const float DriverDamageMult;
var protected const float MomentumMult;
var protected const class<FoxDamageType> CrushedDamageType;
var protected const class<FoxDamageType> RunOverDamageType;
var protected const float MinCrushSpeed;
var protected const float ForceCrushPenetration;
var const ParticleSystem LandParticleSystem;
var const ParticleSystem SpawnLandParticleSystem;
var Actor CachedActorLandedOn;
var int HudMode;
var protected const AkEvent EnterSound;
var protected const AkEvent ExitSound;
var const AkEvent LocalEnterSound;
var const AkEvent LocalExitSound;
var() const transient Vector UIMarkerOffset;
var private array<CrushedPawnInfo> CrushedPawnList;
var private float PawnCrushCoolDown;

replication
{
    // Pos:0x000
    if(bNetDirty && Role == ROLE_Authority)
        bDriving

    // Pos:0x020
    if((bNetDirty && (bNetOwner || Driver == none) || !Driver.bHidden) && Role == ROLE_Authority)
        Driver
}

simulated event ReplicatedEvent(name VarName)
{
    // End:0x24
    if(VarName == 'bDriving')
    {
        DrivingStatusChanged();
    }
    // End:0x90
    else
    {
        // End:0x7D
        if(VarName == 'Driver')
        {
            // End:0x7A
            if((PlayerReplicationInfo != none) && Driver != none)
            {
                Driver.NotifyTeamChanged();
            }
        }
        // End:0x90
        else
        {
            super.ReplicatedEvent(VarName);
        }
    }
    //return;    
}

simulated function NotifyTeamChanged()
{
    // End:0x3F
    if((PlayerReplicationInfo != none) && Driver != none)
    {
        Driver.NotifyTeamChanged();
    }
    super.NotifyTeamChanged();
    //return;    
}

function Suicide()
{
    // End:0x3A
    if(Driver != none)
    {
        Driver.KilledBy(Driver);
    }
    // End:0x45
    else
    {
        KilledBy(self);
    }
    //return;    
}

// Export UFoxVehicle::execGetTargetLocation(FFrame&, void* const)
native simulated function Vector GetTargetLocation(optional Actor RequestedBy, optional bool bRequestAlternateLoc);

simulated function TakeRadiusDamage(Controller InstigatedBy, float BaseDamage, float DamageRadius, class<DamageType> DamageType, float Momentum, Vector HurtOrigin, bool bFullDamage, Actor DamageCauser, optional float DamageFalloffExponent)
{
    DamageFalloffExponent = 1.0;
    // End:0xD2
    if(Role == ROLE_Authority)
    {
        super(Actor).TakeRadiusDamage(InstigatedBy, BaseDamage, DamageRadius, DamageType, Momentum, HurtOrigin, bFullDamage, DamageCauser, DamageFalloffExponent);
        // End:0xD2
        if(Health > 0)
        {
            DriverRadiusDamage(BaseDamage, DamageRadius, InstigatedBy, DamageType, Momentum, HurtOrigin, DamageCauser);
        }
    }
    //return;    
}

function DriverRadiusDamage(float DamageAmount, float DamageRadius, Controller EventInstigator, class<DamageType> DamageType, float Momentum, Vector HitLocation, Actor DamageCauser, optional float DamageFalloffExponent)
{
    DamageFalloffExponent = 1.0;
    // End:0xEC
    if(((((EventInstigator != none) && Driver != none) && bAttachDriver) && !Driver.bCollideActors) && !Driver.bBlockActors)
    {
        Driver.TakeRadiusDamage(EventInstigator, DamageAmount, DamageRadius, DamageType, Momentum, HitLocation, false, DamageCauser, DamageFalloffExponent);
    }
    //return;    
}

function DriverTakeDamage(int Damage, Controller InstigatedBy, Vector HitLocation, Vector Momentum, class<DamageType> DamageType)
{
    // End:0x81
    if(Driver != none)
    {
        Driver.TakeDamage(int(float(Damage) * Driver.IncendiaryProtection), InstigatedBy, HitLocation, Momentum, DamageType);
    }
    //return;    
}

function AddHealDamageStat(FoxOnlineStatsWriteWeaponUse WeaponUsageStats, int HealAmount)
{
    // End:0x5F
    if(WeaponUsageStats != none)
    {
        WeaponUsageStats.IncrementRepairAmount(HealAmount);
        WeaponUsageStats.IncrementHardsuitRepairAmount(HealAmount);
    }
    //return;    
}

function PlayerChangedTeam()
{
    // End:0x3A
    if(Driver != none)
    {
        Driver.KilledBy(Driver);
    }
    // End:0x44
    else
    {
        super.PlayerChangedTeam();
    }
    //return;    
}

simulated function SetBaseEyeheight()
{
    BaseEyeHeight = default.BaseEyeHeight;
    EyeHeight = BaseEyeHeight;
    //return;    
}

simulated event PostBeginPlay()
{
    super.PostBeginPlay();
    // End:0x23
    if(!bDeleteMe)
    {
        AddDefaultInventory();
    }
    //return;    
}

simulated event Destroyed()
{
    local Pawn OldDriver;

    // End:0xE8
    if(Driver != none)
    {
        Driver.LastRenderTime = LastRenderTime;
        // End:0xA4
        if(Role == ROLE_Authority)
        {
            OldDriver = Driver;
            Driver = none;
            OldDriver.DrivenVehicle = none;
            OldDriver.Destroy();
        }
        // End:0xE8
        else
        {
            // End:0xE8
            if(Driver.DrivenVehicle == self)
            {
                Driver.StopDriving(self);
            }
        }
    }
    super.Destroyed();
    //return;    
}

// Export UFoxVehicle::execCanEnterVehicle(FFrame&, void* const)
native function bool CanEnterVehicle(Pawn P);

function bool CanLeaveVehicle()
{
    // End:0x16
    if(SpecialMove == 12)
    {
        return false;
    }
    return true;
    //return ReturnValue;    
}

private final function bool AnySeatAvailable()
{
    return !IsDriven();
    //return ReturnValue;    
}

function bool TryToDrive(Pawn P)
{
    // End:0x1A
    if(!CanEnterVehicle(P))
    {
        return false;
    }
    return DriverEnter(P);
    //return ReturnValue;    
}

protected function bool DriverEnter(Pawn P)
{
    local Controller C;
    local FoxPC PC;

    C = P.Controller;
    Driver = FoxPawn(P);
    Driver.StartDriving(self);
    // End:0x95
    if(Driver.Health <= 0)
    {
        Driver = none;
        return false;
    }
    SetDriving(true);
    C.UnPossess();
    Driver.SetOwner(self);
    C.Possess(self, true);
    // End:0x163
    if(FoxPawn(P).CarriedObject != none)
    {
        FoxPawn(P).CarriedObject.Drop();
    }
    // End:0x1F8
    foreach WorldInfo.AllControllers(class'FoxPC', PC)
    {
        // End:0x1F7
        if(PC.ViewTarget == P)
        {
            PC.SetViewTarget(self);
            SetupSpectateViewFor(PC);
        }        
    }    
    // End:0x23D
    if(PlayerController(C) != none)
    {
        PlayerController(C).GotoState(LandMovementState);
    }
    PC = FoxPC(C);
    // End:0x287
    if(PC != none)
    {
        PC.SetFOVToVehicleSettings();
    }
    return true;
    //return ReturnValue;    
}

function PossessedBy(Controller C, bool bVehicleTransition)
{
    super.PossessedBy(C, bVehicleTransition);
    C.ClientSetRotation(Rotation, true);
    EntryAnnouncement(C);
    NetPriority = 3.0;
    NetUpdateFrequency = 100.0;
    //return;    
}

function EntryAnnouncement(Controller C)
{
    //return;    
}

simulated function AttachDriver(Pawn P)
{
    // End:0x11
    if(!bAttachDriver)
    {
        return;
    }
    // End:0x9E
    if(FoxPawn(P).bIsFlashBanged)
    {
        SetTimer(P.GetTimerRate('EndFlashBangTimer') - P.GetTimerCount('EndFlashBangTimer'), false, 'EndFlashBangTimer');
    }
    P.SetCollision(false, false);
    P.bCollideWorld = false;
    P.SetBase(none);
    P.SetHardAttach(true);
    P.SetPhysics(0);
    // End:0x1A2
    if((P.Mesh != none) && Mesh != none)
    {
        P.Mesh.SetShadowParent(Mesh);
    }
    // End:0x1F2
    if(!bDriverIsVisible)
    {
        P.SetHidden(true);
        P.SetLocation(Location);
    }
    P.SetBase(self);
    P.SetPhysics(0);
    // End:0x24B
    if(EnterSound != none)
    {
        PostAkEvent(EnterSound);
    }
    //return;    
}

simulated function DetachDriver(Pawn P)
{
    // End:0x41
    if(FoxPawn(P) != none)
    {
        FoxPawn(P).ToggleCrouch(false);
    }
    // End:0x64
    if(ExitSound != none)
    {
        PostAkEvent(ExitSound);
    }
    //return;    
}

function Rotator GetExitRotation(Controller C)
{
    local Rotator Rot;

    Rot.Yaw = C.Rotation.Yaw;
    return Rot;
    //return ReturnValue;    
}

event bool DriverLeave(bool bForceLeave)
{
    local Controller C;
    local PlayerController PC;
    local Rotator ExitRotation;
    local FoxPC FPC;

    // End:0x20
    if(Role < ROLE_Authority)
    {
        ScriptTrace();
        return false;
    }
    // End:0x42
    if(!bForceLeave && !CanLeaveVehicle())
    {
        return false;
    }
    // End:0x53
    if(Controller == none)
    {
        return false;
    }
    // End:0x162
    if(Driver != none)
    {
        Driver.SetHardAttach(false);
        Driver.bCollideWorld = true;
        Driver.SetCollision(true, true);
        // End:0x162
        if(!PlaceExitingDriver())
        {
            // End:0x13E
            if(!bForceLeave)
            {
                Driver.SetHardAttach(true);
                Driver.bCollideWorld = false;
                Driver.SetCollision(false, false);
                return false;
            }
            // End:0x162
            else
            {
                Driver.SetLocation(GetTargetLocation());
            }
        }
    }
    ExitRotation = GetExitRotation(Controller);
    SetDriving(false);
    C = Controller;
    // End:0x1E1
    if(C.RouteGoal == self)
    {
        C.RouteGoal = none;
    }
    // End:0x225
    if(C.MoveTarget == self)
    {
        C.MoveTarget = none;
    }
    Controller.UnPossess();
    // End:0x2EE
    foreach WorldInfo.AllControllers(class'FoxPC', FPC)
    {
        // End:0x2ED
        if(FPC.ViewTarget == self)
        {
            FPC.SetViewTarget(Driver);
            Driver.SetupSpectateViewFor(FPC);
        }        
    }    
    // End:0x403
    if((Driver != none) && Driver.Health > 0)
    {
        Driver.SetRotation(ExitRotation);
        Driver.SetOwner(C);
        C.Possess(Driver, true);
        PC = PlayerController(C);
        // End:0x3E3
        if(PC != none)
        {
            PC.ClientSetViewTarget(Driver);
        }
        Driver.StopDriving(self);
    }
    // End:0x425
    if(C == Controller)
    {
        Controller = none;
    }
    FPC = FoxPC(C);
    // End:0x46F
    if(FPC != none)
    {
        FPC.SetFOVToPlayerSettings();
    }
    DriverLeft();
    return true;
    //return ReturnValue;    
}

unreliable server function ServerDriverLeave(bool bForceLeave)
{
    DriverLeave(bForceLeave);
    //return;    
}

simulated function SetInputs(float InForward, float InStrafe, float InUp)
{
    Throttle = InForward;
    Steering = InStrafe;
    Rise = InUp;
    //return;    
}

protected function DriverLeft()
{
    Driver = none;
    PlayerReplicationInfo = none;
    SetDriving(false);
    //return;    
}

private final function bool PlaceExitingDriver(optional Pawn ExitingDriver)
{
    local int I;
    local Vector tryPlace, Extent, HitLocation, HitNormal, ZOffset;

    // End:0x23
    if(ExitingDriver == none)
    {
        ExitingDriver = Driver;
    }
    // End:0x34
    if(ExitingDriver == none)
    {
        return false;
    }
    Extent = ExitingDriver.GetCollisionRadius() * vect(1.0, 1.0, 0.0);
    Extent.Z = ExitingDriver.GetCollisionHeight();
    ZOffset = Extent.Z * vect(0.0, 0.0, 1.0);
    // End:0x28A
    if(ExitPositions.Length > 0)
    {
        I = 0;
        J0xF8:
        // End:0x287 [Loop If]
        if(I < ExitPositions.Length)
        {
            // End:0x170
            if(ExitPositions[0].Z != float(0))
            {
                ZOffset = vect(0.0, 0.0, 1.0) * ExitPositions[0].Z;
            }
            // End:0x1BC
            else
            {
                ZOffset = ExitingDriver.CylinderComponent.default.CollisionHeight * vect(0.0, 0.0, 2.0);
            }
            tryPlace = (Location + ((ExitPositions[I] - ZOffset) >> Rotation)) + ZOffset;
            // End:0x24E
            if(Trace(HitLocation, HitNormal, tryPlace, Location + ZOffset, false, Extent) != none)
            {
            }
            // End:0x279
            else
            {
                // End:0x277
                if(!ExitingDriver.SetLocation(tryPlace))
                {
                }
                // End:0x279
                else
                {
                    return true;
                }
            }
            ++ I;
            // [Loop Continue]
            goto J0xF8;
        }
    }
    // End:0x29E
    else
    {
        return FindAutoExit(ExitingDriver);
    }
    return false;
    //return ReturnValue;    
}

private final function bool FindAutoExit(Pawn ExitingDriver)
{
    local Vector FacingDir, CrossProduct;
    local float PlaceDist;

    FacingDir = vector(Rotation);
    CrossProduct = Normal(FacingDir Cross vect(0.0, 0.0, 1.0));
    // End:0x7E
    if(ExitRadius == float(0))
    {
        ExitRadius = (GetCollisionRadius()) + ExitingDriver.VehicleCheckRadius;
    }
    PlaceDist = ExitRadius + ExitingDriver.GetCollisionRadius();
    return ((TryExitPos(ExitingDriver, ((GetTargetLocation()) + ExitOffset) + (PlaceDist * CrossProduct), false) || TryExitPos(ExitingDriver, ((GetTargetLocation()) + ExitOffset) - (PlaceDist * CrossProduct), false)) || TryExitPos(ExitingDriver, ((GetTargetLocation()) + ExitOffset) - (PlaceDist * FacingDir), false)) || TryExitPos(ExitingDriver, ((GetTargetLocation()) + ExitOffset) + (PlaceDist * FacingDir), false);
    //return ReturnValue;    
}

private final function bool TryExitPos(Pawn ExitingDriver, Vector ExitPos, bool bMustFindGround)
{
    local Vector Slice, HitLocation, HitNormal, StartLocation, NewActorPos;

    local Actor HitActor;

    Slice = ExitingDriver.GetCollisionRadius() * vect(1.0, 1.0, 0.0);
    Slice.Z = 2.0;
    StartLocation = GetTargetLocation();
    // End:0xAC
    if(Trace(HitLocation, HitNormal, ExitPos, StartLocation, false, Slice) != none)
    {
        return false;
    }
    HitActor = Trace(HitLocation, HitNormal, ExitPos - (ExitingDriver.GetCollisionHeight() * vect(0.0, 0.0, 5.0)), ExitPos, true, Slice);
    // End:0x14B
    if(HitActor == none)
    {
        // End:0x138
        if(bMustFindGround)
        {
            return false;
        }
        HitLocation = ExitPos;
    }
    NewActorPos = HitLocation + ((ExitingDriver.GetCollisionHeight() + ExitingDriver.MaxStepHeight) * vect(0.0, 0.0, 1.0));
    // End:0x1EE
    if(PointCheckComponent(Mesh, NewActorPos, ExitingDriver.GetCollisionExtent()))
    {
        return false;
    }
    return ExitingDriver.SetLocation(NewActorPos);
    //return ReturnValue;    
}

function UnPossessed()
{
    NetPriority = default.NetPriority;
    bForceNetUpdate = true;
    NetUpdateFrequency = 8.0;
    super.UnPossessed();
    //return;    
}

function Controller SetKillInstigator(Controller InstigatedBy, class<DamageType> DamageType)
{
    return InstigatedBy;
    //return ReturnValue;    
}

event TakeDamage(int Damage, Controller EventInstigator, Vector HitLocation, Vector Momentum, class<DamageType> DamageType, optional TraceHitInfo HitInfo, optional Actor DamageCauser)
{
    bForceNetUpdate = true;
    // End:0x7C
    if(DamageType != none)
    {
        Damage *= DamageType.static.VehicleDamageScalingFor(self);
        Momentum *= (DamageType.default.VehicleMomentumScaling * MomentumMult);
    }
    super.TakeDamage(Damage, EventInstigator, HitLocation, Momentum, DamageType, HitInfo, DamageCauser);
    //return;    
}

function AdjustDriverDamage(out int Damage, Controller InstigatedBy, Vector HitLocation, out Vector Momentum, class<DamageType> DamageType)
{
    // End:0x1B
    if(InGodMode())
    {
        Damage = 0;
    }
    // End:0x7C
    else
    {
        // End:0x68
        if(class<FoxDamageType_Corrosive>(DamageType) != none)
        {
            Damage *= class<FoxDamageType_Corrosive>(DamageType).default.PercentageToDriver;
        }
        // End:0x7C
        else
        {
            Damage *= DriverDamageMult;
        }
    }
    //return;    
}

function ThrowActiveWeapon(optional bool bDestroyWeap)
{
    //return;    
}

function bool Died(Controller Killer, class<DamageType> DamageType, Vector HitLocation)
{
    // End:0x49
    if(Driver != none)
    {
        Driver.Died(Killer, DamageType, HitLocation);
    }
    // End:0x80
    if(super.Died(Killer, DamageType, HitLocation))
    {
        DriverLeft();
        return true;
    }
    // End:0x82
    else
    {
        return false;
    }
    //return ReturnValue;    
}

function DriverDied(class<DamageType> DamageType)
{
    local Controller C;
    local PlayerReplicationInfo RealPRI;
    local FoxPC PC;

    // End:0x11
    if(Driver == none)
    {
        return;
    }
    WorldInfo.Game.DiscardInventory(Driver);
    C = Controller;
    Driver.StopDriving(self);
    Driver.Controller = C;
    Driver.DrivenVehicle = self;
    // End:0xDB
    if(Controller == none)
    {
        return;
    }
    // End:0x178
    if(PlayerController(Controller) != none)
    {
        Controller.SetLocation(Location);
        PlayerController(Controller).SetViewTarget(Driver);
        PlayerController(Controller).ClientSetViewTarget(Driver);
    }
    // End:0x1F1
    foreach WorldInfo.AllControllers(class'FoxPC', PC)
    {
        // End:0x1F0
        if(PC.KilledByPawn == self)
        {
            PC.KilledByPawn = none;
        }        
    }    
    Controller.UnPossess();
    // End:0x233
    if(Controller == C)
    {
        Controller = none;
    }
    C.Pawn = Driver;
    RealPRI = Driver.PlayerReplicationInfo;
    // End:0x2CF
    if(RealPRI == none)
    {
        Driver.PlayerReplicationInfo = C.PlayerReplicationInfo;
    }
    Driver.PlayerReplicationInfo = RealPRI;
    // End:0x337
    if(FoxPC(C) != none)
    {
        FoxPC(C).SetFOVToPlayerSettings();
    }
    DriverLeft();
    //return;    
}

function NotifyGameKilled(Controller Killer, class<DamageType> DamageType, Vector HitLocation)
{
    //return;    
}

event EncroachedBy(Actor Other)
{
    //return;    
}

function Controller GetCollisionDamageInstigator()
{
    // End:0x1C
    if(Controller != none)
    {
        return Controller;
    }
    // End:0x4D
    else
    {
        return ((Instigator != none) ? Instigator.Controller : none);
    }
    //return ReturnValue;    
}

event RanInto(Actor Other)
{
    CheckRanInto(Other);
    super(Actor).RanInto(Other);
    //return;    
}

event Bump(Actor Other, PrimitiveComponent OtherComp, Vector HitNormal)
{
    CheckRanInto(Other);
    super.Bump(Other, OtherComp, HitNormal);
    //return;    
}

event HitWall(Vector HitNormal, Actor Wall, PrimitiveComponent WallComp)
{
    // End:0x6F
    if(IsDoingSpecialMove(12) && IsLocallyControlled())
    {
        FoxPC(Controller).CancelSprintButtonPress();
        FoxPC(Controller).StopCurrentSpecialMove();
    }
    super.HitWall(HitNormal, Wall, WallComp);
    //return;    
}

function bool CheckRanInto(Actor Other)
{
    local Pawn P;
    local Vector PushVelocity, CheckExtent;
    local bool bSlowEncroach, bDeepEncroach;

    P = Pawn(Other);
    // End:0x2D
    if(P == none)
    {
        return false;
    }
    bSlowEncroach = VSize(Velocity) < MinCrushSpeed;
    // End:0x18F
    if(bSlowEncroach)
    {
        CheckExtent.X = P.CylinderComponent.CollisionRadius - ForceCrushPenetration;
        CheckExtent.Y = CheckExtent.X;
        CheckExtent.Z = P.CylinderComponent.CollisionHeight - ForceCrushPenetration;
        bDeepEncroach = PointCheckComponent(CollisionComponent, P.Location, CheckExtent);
    }
    // End:0x353
    if((((((Other == Instigator) && !bDeepEncroach) || Vehicle(Other) != none) || Other.Role != ROLE_Authority) || !Other.bCollideActors && !Other.bBlockActors) || bSlowEncroach && !bDeepEncroach)
    {
        // End:0x351
        if((P.Velocity Dot (Location - P.Location)) > float(0))
        {
            PushVelocity = Normal(P.Location - Location) * float(200);
            PushVelocity.Z = 100.0;
            P.AddVelocity(PushVelocity, Location, RunOverDamageType);
        }
        return false;
    }
    // End:0x3F3
    if(P.Base == self)
    {
        RanInto(P);
        // End:0x3CD
        if(P.Base != none)
        {
            P.JumpOffPawn();
        }
        // End:0x3F3
        if(P.Base == none)
        {
            return false;
        }
    }
    PancakeOther(P);
    return false;
    //return ReturnValue;    
}

event bool EncroachingOn(Actor Other)
{
    CheckRanInto(Other);
    return super(Pawn).EncroachingOn(Other);
    //return ReturnValue;    
}

function PancakeOther(Pawn Other)
{
    // End:0xA0
    if(FoxVehicle(Other) != none)
    {
        Other.TakeDamage(1000, GetCollisionDamageInstigator(), Other.Location, Velocity * Mass, ((Driver != none) ? RunOverDamageType : CrushedDamageType));
    }
    // End:0x125
    else
    {
        Other.TakeDamage(10000, GetCollisionDamageInstigator(), Other.Location, Velocity * Mass, ((Driver != none) ? RunOverDamageType : CrushedDamageType));
    }
    //return;    
}

function CrushedBy(Pawn OtherPawn)
{
    // End:0x5C
    if(FoxVehicle(OtherPawn) != none)
    {
        Died(FoxVehicle(OtherPawn).GetCollisionDamageInstigator(), CrushedDamageType, TakeHitLocation);
    }
    //return;    
}

simulated event Vector GetEntryLocation()
{
    return Location;
    //return ReturnValue;    
}

simulated function SetDriving(const bool bNewDriving)
{
    // End:0x38
    if(bDriving != bNewDriving)
    {
        bDriving = bNewDriving;
        DrivingStatusChanged();
    }
    //return;    
}

function HandleDeadVehicleDriver()
{
    //return;    
}

simulated function DrivingStatusChanged()
{
    // End:0x31
    if(IsAliveAndWell() && Role == ROLE_Authority)
    {
        DoSpecialMove(, true);
    }
    SetEmissives();
    // End:0xA5
    if(!bDriving)
    {
        Throttle = 0.0;
        Steering = 0.0;
        Rise = 0.0;
        Velocity = vect(0.0, 0.0, 0.0);
        Acceleration = vect(0.0, 0.0, 0.0);
    }
    //return;    
}

event Landed(Vector HitNormal, Actor FloorActor)
{
    // End:0x2C
    if(bDriving)
    {
        super.Landed(HitNormal, FloorActor);
    }
    // End:0xE7
    else
    {
        SetCollisionSize(default.CylinderComponent.CollisionRadius, default.CylinderComponent.CollisionHeight);
        CachedActorLandedOn = FloorActor;
        HurtRadius(10000.0, default.CylinderComponent.CollisionRadius, CrushedDamageType, 0.0, Location);
        // End:0xE7
        if(Role == ROLE_Authority)
        {
            DoSpecialMove(ROLE_Authority);
        }
    }
    //return;    
}

simulated function PlaySpawnLandEffects()
{
    //return;    
}

// Export UFoxVehicle::execIsDriven(FFrame&, void* const)
native simulated function bool IsDriven();

function NotifyDriverTakeHit(Controller InstigatedBy, Vector HitLocation, int Damage, class<DamageType> DamageType, Vector Momentum)
{
    //return;    
}

simulated function ZeroMovementVariables()
{
    super(Pawn).ZeroMovementVariables();
    Steering = 0.0;
    Rise = 0.0;
    Throttle = 0.0;
    //return;    
}

// Export UFoxVehicle::execGetDriver(FFrame&, void* const)
native simulated function FoxPawn GetDriver();

simulated function ToggleHRVEffects(const bool bEnabled, const bool bFriendly, const bool bHardSuitHRV)
{
    local int I;
    local MaterialInstanceConstant MIC;

    // End:0x5A
    if(((IsInvincible()) || !IsAliveAndWell() && bEnabled) && Mesh.DepthPriorityGroup == 1)
    {
        return;
    }
    // End:0xBF
    if(bHardSuitHRV)
    {
        // End:0xB2
        if((bEnabled && !IsLocallyControlled()) && !IsBeingFPSpectatedByLocalController())
        {
            // End:0xAF
            if(HardSuitPingPSC == none)
            {
                CreateHardSuitHRVPS();
            }
        }
        // End:0xBD
        else
        {
            HardSuitPingPSC = none;
        }
        return;
    }
    bHRVMaterialsApplied = bEnabled;
    SetHVTPSVisibility(bEnabled);
    I = 0;
    J0xF3:
    // End:0x1AF [Loop If]
    if((I < Mesh.GetNumElements()) && I < TPCamoSwapLowerBodyMeshMICs.Length)
    {
        MIC = ((bEnabled) ? GetHRVMaterial() : TPCamoSwapLowerBodyMeshMICs[I]);
        Mesh.SetMaterial(I, MIC);
        ++ I;
        // [Loop Continue]
        goto J0xF3;
    }
    // End:0x285
    if(UpperBodyMesh != none)
    {
        I = 0;
        J0x1C9:
        // End:0x285 [Loop If]
        if((I < UpperBodyMesh.GetNumElements()) && I < TPCamoSwapLowerBodyMeshMICs.Length)
        {
            MIC = ((bEnabled) ? GetHRVMaterial() : TPCamoSwapUpperBodyMeshMICs[I]);
            UpperBodyMesh.SetMaterial(I, MIC);
            ++ I;
            // [Loop Continue]
            goto J0x1C9;
        }
    }
    I = 0;
    J0x290:
    // End:0x2E9 [Loop If]
    if(I < ThirdPersonWeaponAttachments.Length)
    {
        ThirdPersonWeaponAttachments[I].ToggleHRVEffects(bEnabled);
        ++ I;
        // [Loop Continue]
        goto J0x290;
    }
    // End:0x323
    if(LightEnvironment != none)
    {
        LightEnvironment.SetEnabled(!bEnabled);
    }
    // End:0x34E
    if(bEnabled && IsAliveAndWell())
    {
        SetDepthPriorityGroup(2);
    }
    // End:0x35A
    else
    {
        SetDepthPriorityGroup(1);
    }
    //return;    
}

simulated function SetGearFromLoadout()
{
    SetupCamoParams();
    //return;    
}

simulated function SetEmissives()
{
    local int I;
    local LinearColor EmissiveColor;

    // End:0x71
    if(bDriving && PlayerReplicationInfo != none)
    {
        EmissiveColor = FoxGRI(WorldInfo.GRI).GetEmissiveColorForTeam(PlayerReplicationInfo);
    }
    // End:0x99
    else
    {
        EmissiveColor = MakeLinearColor(0.0, 0.0, 0.0, 1.0);
    }
    I = 0;
    J0xA4:
    // End:0x105 [Loop If]
    if(I < FPCamoSwapMICs.Length)
    {
        FPCamoSwapMICs[I].SetVectorParameterValue(EmissiveParamName, EmissiveColor);
        ++ I;
        // [Loop Continue]
        goto J0xA4;
    }
    I = 0;
    J0x110:
    // End:0x171 [Loop If]
    if(I < TPCamoSwapLowerBodyMeshMICs.Length)
    {
        TPCamoSwapLowerBodyMeshMICs[I].SetVectorParameterValue(EmissiveParamName, EmissiveColor);
        ++ I;
        // [Loop Continue]
        goto J0x110;
    }
    I = 0;
    J0x17C:
    // End:0x1DD [Loop If]
    if(I < TPCamoSwapUpperBodyMeshMICs.Length)
    {
        TPCamoSwapUpperBodyMeshMICs[I].SetVectorParameterValue(EmissiveParamName, EmissiveColor);
        ++ I;
        // [Loop Continue]
        goto J0x17C;
    }
    //return;    
}

function AddCrushedPawn(FoxPawn inPawn)
{
    local CrushedPawnInfo NewInfo;

    NewInfo.CrushedPawn = inPawn;
    NewInfo.CrushedTime = WorldInfo.TimeSeconds;
    CrushedPawnList.AddItem(NewInfo);
    //return;    
}

function bool WasPawnAlreadyCrushed(FoxPawn inPawn)
{
    local int I, ExpiredCount;
    local float ExpirationTime;
    local bool Ret;

    ExpiredCount = 0;
    Ret = false;
    ExpirationTime = WorldInfo.TimeSeconds - PawnCrushCoolDown;
    I = 0;
    J0x55:
    // End:0x100 [Loop If]
    if(I < CrushedPawnList.Length)
    {
        // End:0xAF
        if(CrushedPawnList[I].CrushedTime < ExpirationTime)
        {
            ++ ExpiredCount;
        }
        // End:0xF2
        else
        {
            // End:0xF2
            if(CrushedPawnList[I].CrushedPawn == inPawn)
            {
                Ret = true;
                // [Explicit Break]
                goto J0x100;
            }
        }
        ++ I;
        J0x100:
        // [Loop Continue]
        goto J0x55;
    }
    // End:0x124
    if(ExpiredCount > 0)
    {
        CrushedPawnList.Remove(0, ExpiredCount);
    }
    return Ret;
    //return ReturnValue;    
}

simulated function SetPawnAttributes()
{
    //return;    
}

simulated function CalculatePawnMods()
{
    //return;    
}

// Export UFoxVehicle::execGetBaseAimRotation(FFrame&, void* const)
native simulated function Rotator GetBaseAimRotation();

defaultproperties
{
    bAttachDriver=true
    bDoExtraNetRelevancyTraces=true
    MomentumMult=1.0
    MinCrushSpeed=20.0
    ForceCrushPenetration=10.0
    PawnCrushCoolDown=5.0
    bHasGore=false
    EnemyHVTMaterialInstance=MaterialInstanceConstant'Default__FoxVehicle.EnemyHVTMaterialInstance0'
    JamEnemyHVTMaterialInstance=MaterialInstanceConstant'Default__FoxVehicle.JamEnemyHVTMaterialInstance0'
    FriendlyHVTMaterialInstance=MaterialInstanceConstant'Default__FoxVehicle.FriendlyHVTMaterialInstance0'
    NuetralHVTMaterialInstance=MaterialInstanceConstant'Default__FoxVehicle.NuetralHVTMaterialInstance0'
    EnemyIRMaterialInstance=MaterialInstanceConstant'Default__FoxVehicle.EnemyIRMaterialInstance0'
    FriendlyIRMaterialInstance=MaterialInstanceConstant'Default__FoxVehicle.FriendlyIRMaterialInstance0'
    NuetralIRMaterialInstance=MaterialInstanceConstant'Default__FoxVehicle.NuetralIRMaterialInstance0'
    LightEnvironment=DynamicLightEnvironmentComponent'Default__FoxVehicle.TPLightEnvironment'
    TakeDamageCameraShake=CameraShake'Default__FoxVehicle.Shake0'
    SpecialMoveClasses(0)=none
    SpecialMoveClasses(1)=class'FoxSpecialMove_Jump'
    SpecialMoveClasses(2)=class'FoxSpecialMove_Land'
    SpecialMoveClasses(3)=class'FoxSpecialMove_VehicleSpawnLand'
    SpecialMoveClasses(4)=class'FoxSpecialMove_EnterLadder'
    SpecialMoveClasses(5)=class'FoxSpecialMove_ExitLadder'
    SpecialMoveClasses(6)=class'FoxSpecialMove_Injured'
    SpecialMoveClasses(7)=class'FoxSpecialMove_Revive'
    SpecialMoveClasses(8)=class'FoxSpecialMove_TauntFullBody'
    SpecialMoveClasses(9)=class'FoxSpecialMove_TauntUpperBody'
    SpecialMoveClasses(10)=class'FoxSpecialMove_TauntUpperBodyWalk'
    SpecialMoveClasses(11)=class'FoxSpecialMove_Idle'
    SpecialMoveClasses(12)=none
    SpecialMoveClasses(13)=class'FoxSpecialMove_DeathAnim'
    SpecialMoveClasses(14)=class'FoxSpecialMove_WeaponDeathAnim'
    SpecialMoveClasses(15)=class'FoxSpecialMove_ToggleVisorOn'
    SpecialMoveClasses(16)=class'FoxSpecialMove_ToggleVisorOff'
    SpecialMoveClasses(17)=class'FoxSpecialMove_MeleeLunge'
    SpecialMoveClasses(18)=class'FoxSpecialMove_CaptureControlPoint'
    SpecialMoveClasses(19)=class'FoxSpecialMove_HackFail'
    SpecialMoveClasses(20)=none
    SpecialMoveClasses(21)=class'FoxSpecialMove_MeleeLungeBlind'
    SpecialMoveClasses(22)=class'FoxSpecialMove_GroundPound'
    SpecialMoveClasses(23)=class'FoxSpecialMove_HulkCharge'
    SpecialMoveClasses(24)=class'FoxSpecialMove_AIEntry'
    SpecialMoveClasses(25)=class'FoxSpecialMove_Dodge'
    SpecialMoveClasses(26)=class'FoxSpecialMove_Stagger'
    SpecialMoveClasses(27)=class'FoxSpecialMove_Trip'
    SpecialMoveClasses(28)=class'FoxSpecialMove_Trip'
    SpecialMoveClasses(29)=class'FoxSpecialMove_Trip'
    SpecialMoveClasses(30)=class'FoxSpecialMove_Trip'
    SpecialMoveClasses(31)=class'FoxSpecialMove_Trip'
    SpecialMoveClasses(32)=class'FoxSpecialMove_PlantBomb'
    SpecialMoveClasses(33)=class'FoxSpecialMove_RemoveBomb'
    DamageProfiles(0)=(TakeDamageCameraShake=CameraShake'Default__FoxVehicle.Shake1',DamageAmount=40)
    DamageProfiles(1)=(TakeDamageCameraShake=CameraShake'Default__FoxVehicle.Shake2',DamageAmount=150)
    DamageProfiles(2)=(TakeDamageCameraShake=CameraShake'Default__FoxVehicle.Shake3',DamageAmount=1500)
    bDontPossess=true
    bPathfindsAsVehicle=true
    bReplicateHealthToAll=true
    LandMovementState=PlayerDriving
    begin object name=PawnSkeletalMeshComponent class=SkeletalMeshComponent
        ReplacementPrimitive=none
        LightEnvironment=DynamicLightEnvironmentComponent'Default__FoxVehicle.TPLightEnvironment'
    object end
    // Reference: SkeletalMeshComponent'Default__FoxVehicle.PawnSkeletalMeshComponent'
    Mesh=PawnSkeletalMeshComponent
    begin object name=CollisionCylinder class=CylinderComponent
        ReplacementPrimitive=none
    object end
    // Reference: CylinderComponent'Default__FoxVehicle.CollisionCylinder'
    CylinderComponent=CollisionCylinder
    VehicleCheckRadius=250.0
    begin object name=CollisionCylinder class=CylinderComponent
        ReplacementPrimitive=none
    object end
    // Reference: CylinderComponent'Default__FoxVehicle.CollisionCylinder'
    Components(0)=CollisionCylinder
    Components(1)=DynamicLightEnvironmentComponent'Default__FoxVehicle.TPLightEnvironment'
    begin object name=PawnSkeletalMeshComponent class=SkeletalMeshComponent
        ReplacementPrimitive=none
        LightEnvironment=DynamicLightEnvironmentComponent'Default__FoxVehicle.TPLightEnvironment'
    object end
    // Reference: SkeletalMeshComponent'Default__FoxVehicle.PawnSkeletalMeshComponent'
    Components(2)=PawnSkeletalMeshComponent
    Components(3)=DynamicLightEnvironmentComponent'Default__FoxVehicle.FPLightEnvironmentObj'
    begin object name=CollisionCylinder class=CylinderComponent
        ReplacementPrimitive=none
    object end
    // Reference: CylinderComponent'Default__FoxVehicle.CollisionCylinder'
    CollisionComponent=CollisionCylinder
}