/*******************************************************************************
 * FoxBillboardUIRepairTarget generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxBillboardUIRepairTarget extends FoxBillboardUIPawnTargetBase within GFxMoviePlayer
    native(HUD)
    config(HUD);

defaultproperties
{
    symbolname="billBoard_repairTarget"
    TacticalSpecificClassName=RepairGunGear
}