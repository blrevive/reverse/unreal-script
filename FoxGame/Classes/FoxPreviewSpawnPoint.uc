/*******************************************************************************
 * FoxPreviewSpawnPoint generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxPreviewSpawnPoint extends Keypoint
    native(Camera)
    hidecategories(Navigation);

var() bool bIsForRenderTarget;

defaultproperties
{
    Components(0)=none
    begin object name=MyLightEnvironment class=DynamicLightEnvironmentComponent
        bEnabled=false
    object end
    // Reference: DynamicLightEnvironmentComponent'Default__FoxPreviewSpawnPoint.MyLightEnvironment'
    Components(1)=MyLightEnvironment
    Components(2)=none
}