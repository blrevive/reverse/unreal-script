/*******************************************************************************
 * FoxWeapon_HealBase generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxWeapon_HealBase extends FoxWeapon_TargetBase
    abstract
    native(Weapon)
    config(Weapon)
    hidecategories(Navigation);

var const class<DamageType> EnemyDamageType;
var const class<DamageType> FriendlyDamageType;
var config int FriendlyHealAmount;
var config int SelfHealAmount;
var config int EnemyHealAmount;
var config int AmmoPerShot;

// Export UFoxWeapon_HealBase::execGetFormattedWeaponStats(FFrame&, void* const)
native function bool GetFormattedWeaponStats(out array<StatPanelEntry> StatEntries);

simulated function ProcessInstantHit(byte FiringMode, ImpactInfo Impact, optional int NumHits)
{
    local FoxPawn HitPawn;
    local int LocalHealAmount;

    HitPawn = FoxPawn(Impact.HitActor);
    // End:0x68
    if((HitPawn == none) || HitPawn.bIsMachine == true)
    {
        return;
    }
    // End:0x152
    if((FoxPRI(Instigator.PlayerReplicationInfo) != none) && FoxPRI(Instigator.PlayerReplicationInfo).WeaponUsageStats != none)
    {
        FoxPRI(Instigator.PlayerReplicationInfo).WeaponUsageStats.IncrementRoundsHit(FoxPawn(Instigator).CurrentZoomState);
    }
    // End:0x361
    if(WorldInfo.GRI.OnSameTeam(Instigator, HitPawn))
    {
        // End:0x35E
        if((HitPawn.Health < HitPawn.HealthMax) && WorldInfo.Game != none)
        {
            // End:0x2F3
            if(((HitPawn.HealthMax - HitPawn.PermanentHealthDamage) - HitPawn.Health) > HitPawn.PendingHealAmount(FriendlyDamageType))
            {
                FoxGame(WorldInfo.Game).ScoreEvent(FoxPRI(Instigator.PlayerReplicationInfo), 17);
            }
            LocalHealAmount = FriendlyHealAmount;
            class<FoxDamageType_HOT>(FriendlyDamageType).static.ApplyEffect(HitPawn, LocalHealAmount, Instigator.Controller);
        }
    }
    // End:0x445
    else
    {
        // End:0x445
        if(ShouldRegisterDamage(Impact.HitActor))
        {
            HitPawn.TakeDamage(EnemyHealAmount, Instigator.Controller, Impact.HitLocation, InstantHitMomentum[FiringMode] * Impact.RayDir, EnemyDamageType, Impact.HitInfo, self);
        }
    }
    // End:0x48F
    if(HitPawn != none)
    {
        HitPawn.TakeDamageStartLoc = Impact.StartTrace;
    }
    //return;    
}

simulated function CustomFire()
{
    local ImpactInfo Impact;

    GetPawnToMelee(false, Impact);
    SetFlashLocation(Impact.HitLocation);
    ProcessInstantHit(0, Impact, 1);
    //return;    
}

simulated event StartMelee()
{
    //return;    
}

simulated function bool HasAmmo(byte FireModeNum, optional int Amount)
{
    Amount = 1;
    // End:0x16
    if(!bConsumeAmmo)
    {
        return true;
    }
    // End:0x54
    if(FireModeNum == 0)
    {
        // End:0x51
        if(float(AmmoUsedCount + AmmoPerShot) > (GetMagazineSize()))
        {
            return false;
        }
    }
    // End:0x65
    else
    {
        // End:0x65
        if(AmmoUsedCount > 0)
        {
            return false;
        }
    }
    return true;
    //return ReturnValue;    
}

simulated function StepZoom()
{
    // End:0x8A
    if(((((HasAmmo(2)) && !IsFiring()) && !IsInState('Inactive')) && FiringStatesArray[2] != 'None') && FoxPawn(Instigator).CanMelee())
    {
        StartFire(2);
    }
    //return;    
}

simulated function ConsumeAmmo(byte FireModeNum)
{
    // End:0xCE
    if(bConsumeAmmo)
    {
        // End:0x60
        if(FireModeNum == 0)
        {
            AmmoUsedCount += AmmoPerShot;
            SimulatedAmmoUsedCount += AmmoPerShot;
            AmmoLeftCount -= AmmoPerShot;
        }
        // End:0x97
        else
        {
            AmmoUsedCount = int(GetMagazineSize());
            SimulatedAmmoUsedCount = int(GetMagazineSize());
            AmmoLeftCount = 0;
        }
        // End:0xCE
        if(Role == ROLE_Authority)
        {
            SetTimer(RechargeTime / (GetMagazineSize()), true, 'TimerAddAmmo');
        }
    }
    //return;    
}

simulated function DoSelfHeal()
{
    //return;    
}

reliable server function ServerStartMeleeAnim()
{
    StartMeleeAnim();
    //return;    
}

simulated state AttackSelf
{
    ignores StopMeleeAnim;

    simulated function BeginState(name PreviousStateName)
    {
        StopZoomMode();
        SetCurrentFireMode(2);
        ClearAllPendingFire();
        FoxPawn(Instigator).UpdateHandsIK();
        SetTimer(0.70, false, 'DoSelfHeal');
        StartMeleeAnim();
        //return;        
    }

    simulated function EndState(name NextStateName)
    {
        SetCurrentFireMode(0);
        FoxPawn(Instigator).UpdateHandsIK();
        FoxInventoryManager(Instigator.InvManager).UnLockWeaponActionSemaphore();
        ClearFlashCount();
        ClearFlashLocation();
        NotifyWeaponFinishedFiring(CurrentFireMode);
        //return;        
    }

    simulated function DoSelfHeal()
    {
        local int LocalHealAmount;

        // End:0x1A7
        if((((((HasAmmo(2)) && Instigator != none) && Instigator.Physics == 1) && Instigator.Controller != none) && FoxPC(Instigator.Controller) != none) && !FoxPC(Instigator.Controller).IsHRVActive())
        {
            ConsumeAmmo(2);
            // End:0x11F
            if(Instigator.IsLocallyControlled())
            {
                StartFiringSound(0);
            }
            // End:0x1A7
            if(Role == ROLE_Authority)
            {
                LocalHealAmount = SelfHealAmount;
                class<FoxDamageType_HOT>(FriendlyDamageType).static.ApplyEffect(FoxPawn(Instigator), LocalHealAmount, Instigator.Controller);
            }
        }
        global.StopMeleeAnim();
        GotoState('Active');
        //return;        
    }
    stop;    
}

defaultproperties
{
    RechargeTime=15.0
    bCanHelpFleshies=true
    bCanHurtFleshies=true
    bCanHelpInstigator=true
    HelpString="Heal"
    HurtString="Poison"
    bCanMeleeTeammates=true
    bSupportsBarrelMods=false
    bSupportsMagazineMods=false
    bSupportsMuzzleMods=false
    bSupportsStockMods=false
    bSupportsGripMods=false
    FireFeedbackWaveform=ForceFeedbackWaveform'Default__FoxWeapon_HealBase.ForceFeedbackWaveformFire'
    TightAimCameraShake=CameraShake'Default__FoxWeapon_HealBase.Shake0'
    NormalCameraShake=CameraShake'Default__FoxWeapon_HealBase.Shake1'
    MuzzleFlashLight=PointLightComponent'Default__FoxWeapon_HealBase.LightComponent0'
    begin object name=LensComp class=LensFlareComponent
        NextTraceTime=-0.15387430
        ReplacementPrimitive=none
    object end
    // Reference: LensFlareComponent'Default__FoxWeapon_HealBase.LensComp'
    MuzzleLensFlare=LensComp
    AN_Melee=(TP_Anim=(AnimName=(None,None,AttackSelf_A)),FP_Anim=AttackSelf_A,WP_Anim=AttackSelf_A)
    AN_NormalFire=(FP_Anim=Attack_A)
    FiringStatesArray=/* Array type was not detected. */
    WeaponFireTypes=/* Array type was not detected. */
    begin object name=WeaponMesh class=FoxFirstPersonSkeletalMeshComponent
        ReplacementPrimitive=none
    object end
    // Reference: FoxFirstPersonSkeletalMeshComponent'Default__FoxWeapon_HealBase.WeaponMesh'
    Mesh=WeaponMesh
}