/*******************************************************************************
 * FoxDialogBoxBase generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxDialogBoxBase extends Object within FoxUI
    abstract
    native(UI)
    config(UI);

enum EDialogBoxConfirmType
{
    EDBCT_OK,
    EDBCT_CANCEL,
    EDBCT_MISC,
    EDBCT_MAX
};

struct native InputBoxVerifyTag
{
    var string TagLabel;
    var bool bIsValid;

    structdefaultproperties
    {
        TagLabel=""
        bIsValid=false
    }
};

struct native InputBoxVerify
{
    var string ValidVerifyTagColor;
    var string InvalidVerifyTagColor;
    var string BoundObjectPath;

    structdefaultproperties
    {
        ValidVerifyTagColor=""
        InvalidVerifyTagColor=""
        BoundObjectPath=""
    }
};

var const config string AcceptIcon;
var const config string CancelIcon;
var const config string AcceptButtonIDPostFix;
var const config string CancelButtonIDPostFix;
var const config string MiscButtonIDPostFix;
var const config string DialogLocSectionName;
var const config string DefaultValidVerifyTagColor;
var const config string DefaultInvalidVerifyTagColor;
var const AkEvent OpenDialogEvent;
var protected bool bAllowEscape;
var bool bErrorDialogBox;
var protected string ObjectPath;
var protected const localized string LocAcceptLabel;
var protected const localized string LocAcceptLabel_X;
var protected const localized string LocStringConfirm;
var protected const localized string LocCancelLabel;
var protected const localized string LocCancelLabel_B;
var protected const localized string LocVerifyLabel;
var const array<string> CurrencyColors;
var int CachedGPAmount;
var int CachedZPAmount;
var AkEvent FanFare_ItemReveal_Audio;
var delegate<DialogButtonPressedDelegate> __DialogButtonPressedDelegate__Delegate;
var delegate<StringInputDelegate> __StringInputDelegate__Delegate;

delegate DialogButtonPressedDelegate(FoxDialogBoxBase.EDialogBoxConfirmType bAccepted)
{
    //return;    
}

delegate StringInputDelegate(string InputString)
{
    //return;    
}

// Export UFoxDialogBoxBase::execUpdateSaleTimers(FFrame&, void* const)
native function UpdateSaleTimers();

protected function BindDialogOld(optional delegate<DialogButtonPressedDelegate> OnDialogButtonPressed, optional string BoundObjectPath)
{
    // End:0x99
    if(BoundObjectPath == "")
    {
        // End:0x85
        if((Outer.PCOwner != none) && OpenDialogEvent != none)
        {
            Outer.PCOwner.PostAkEvent(OpenDialogEvent, true);
        }
        BoundObjectPath = as_addDialogPanel();
    }
    BindObject(BoundObjectPath);
    Outer.Advance(0.0);
    __DialogButtonPressedDelegate__Delegate = OnDialogButtonPressed;
    //return;    
}

protected function BindDialog(GFxObject context, string Linkage)
{
    local string BoundObjectPath;

    // End:0x73
    if((Outer.PCOwner != none) && OpenDialogEvent != none)
    {
        Outer.PCOwner.PostAkEvent(OpenDialogEvent, true);
    }
    BoundObjectPath = as_makeDialogBox(context, Linkage);
    BindObject(BoundObjectPath);
    Outer.Advance(0.0);
    //return;    
}

protected function BindDialogButtonPressedDelegate(delegate<DialogButtonPressedDelegate> OnDialogButtonPressed)
{
    __DialogButtonPressedDelegate__Delegate = OnDialogButtonPressed;
    //return;    
}

function CloseDialog()
{
    as_removeDialogBox();
    ObjectPath = "";
    // End:0x5A
    if(Outer.DialogBox == self)
    {
        Outer.DialogBox = none;
    }
    //return;    
}

protected function BindObject(string InObjectPath)
{
    ObjectPath = InObjectPath;
    //return;    
}

protected function GFxObject GetBoundObject()
{
    return Outer.GetVariableObject(ObjectPath);
    //return ReturnValue;    
}

protected function string GetBoundObjectPath()
{
    return ObjectPath;
    //return ReturnValue;    
}

protected function ToggleAllowEscape(bool bShouldAllowEscape)
{
    bAllowEscape = bShouldAllowEscape;
    //return;    
}

protected function NotifyButtonPressed(FoxDialogBoxBase.EDialogBoxConfirmType Result)
{
    DialogButtonPressedDelegate(Result);
    //return;    
}

protected function ShowVerifyLabel(InputBoxVerify InputBox, InputBoxVerifyTag VerifyTag)
{
    local GFxObject BoundObject;
    local array<ASValue> VerifyLabelArgs;

    BoundObject = Outer.GetVariableObject(InputBox.BoundObjectPath);
    // End:0x23F
    if(BoundObject != none)
    {
        VerifyLabelArgs.Add(3);
        VerifyLabelArgs[0].Type = 4;
        VerifyLabelArgs[0].S = VerifyTag.TagLabel;
        VerifyLabelArgs[1].Type = 4;
        VerifyLabelArgs[1].S = ((VerifyTag.bIsValid) ? InputBox.ValidVerifyTagColor : InputBox.InvalidVerifyTagColor);
        VerifyLabelArgs[2].Type = 5;
        VerifyLabelArgs[2].B = VerifyTag.bIsValid;
        BoundObject.Invoke("showVerifyLabel", VerifyLabelArgs);
        BoundObject.Invoke("showVerifyLabel", VerifyLabelArgs);
        J0x23F:
    }
    // End:0x23F
    else
    {
    }
    //return;    
}

private final function ei_closeDialog()
{
    // End:0x23
    if(bAllowEscape)
    {
        CloseDialog();
        NotifyButtonPressed(1);
    }
    //return;    
}

protected function string as_addDialogPanel()
{
    return Outer.ActionScriptString("_root.addDialogPanel");
    //return ReturnValue;    
}

protected function string as_makeDialogBox(GFxObject context, string Linkage)
{
    return Outer.ActionScriptString("_root.makeDialogBox");
    //return ReturnValue;    
}

private final function as_removeDialogBox()
{
    local GFxObject BoundObject;

    BoundObject = GetBoundObject();
    // End:0x53
    if(BoundObject != none)
    {
        BoundObject.ActionScriptVoid("removeDialogBox");
    }
    //return;    
}

protected event string as_addDialogButton(string ButtonLabel, string ExtInterfaceClickFunc, string ExtInterfaceClickArg, optional bool bHasInput)
{
    local GFxObject BoundObject;

    BoundObject = GetBoundObject();
    // End:0x5A
    if(BoundObject != none)
    {
        return BoundObject.ActionScriptString("addDialogBasicButton");
    }
    return "";
    //return ReturnValue;    
}

protected event string as_addDialogPrompt(string ButtonLabel)
{
    local GFxObject BoundObject;

    BoundObject = GetBoundObject();
    // End:0x5A
    if(BoundObject != none)
    {
        return BoundObject.ActionScriptString("addDialogButtonPrompt");
    }
    return "";
    //return ReturnValue;    
}

protected function string ui_addDialogButton(string ButtonLabel, string ExtInterfaceClickFunc, string ExtInterfaceClickArg, optional bool bHasInput, optional string ButtonID)
{
    local string ButtonPath;
    local GFxObject ButtonObject;

    ButtonPath = as_addDialogButton(ButtonLabel, ExtInterfaceClickFunc, ExtInterfaceClickArg, bHasInput);
    ButtonObject = Outer.GetVariableObject(ButtonPath);
    // End:0xB0
    if(ButtonObject != none)
    {
        ButtonObject.SetString("uniqueID", ButtonID);
    }
    return ButtonPath;
    //return ReturnValue;    
}

protected function ui_setConfirmButton(optional string ButtonLabel, optional string ButtonIconPath, optional string ExtInterfaceClickFunc, optional string ExtInterfaceClickArg, optional bool bFocused, optional bool bTabEnabled, optional bool bColorIcon, optional bool bDisabled)
{
    bTabEnabled = true;
    bColorIcon = true;    
    ui_setDialogButton(0.0, ButtonLabel, ButtonIconPath, ExtInterfaceClickFunc, ExtInterfaceClickArg, bFocused, bTabEnabled, bColorIcon, bDisabled);
    //return;    
}

protected function ui_setCancelButton(optional string ButtonLabel, optional string ButtonIconPath, optional string ExtInterfaceClickFunc, optional string ExtInterfaceClickArg, optional bool bFocused, optional bool bTabEnabled, optional bool bColorIcon, optional bool bDisabled)
{
    bTabEnabled = true;
    bColorIcon = true;    
    ui_setDialogButton(1.0, ButtonLabel, ButtonIconPath, ExtInterfaceClickFunc, ExtInterfaceClickArg, bFocused, bTabEnabled, bColorIcon, bDisabled);
    //return;    
}

protected function ui_setMiscButton(optional string ButtonLabel, optional string ButtonIconPath, optional string ExtInterfaceClickFunc, optional string ExtInterfaceClickArg, optional bool bFocused, optional bool bTabEnabled, optional bool bColorIcon, optional bool bDisabled)
{
    bTabEnabled = true;
    bColorIcon = true;    
    ui_setDialogButton(2.0, ButtonLabel, ButtonIconPath, ExtInterfaceClickFunc, ExtInterfaceClickArg, bFocused, bTabEnabled, bColorIcon, bDisabled);
    //return;    
}

protected function ui_setDialogButton(float ButtonIndex, optional string ButtonLabel, optional string ButtonIconPath, optional string ExtInterfaceClickFunc, optional string ExtInterfaceClickArg, optional bool bFocused, optional bool bTabEnabled, optional bool bColorIcon, optional bool bDisabled)
{
    local GFxObject DataObject;

    bTabEnabled = true;
    bColorIcon = true;    
    DataObject = Outer.CreateObject("Object");
    DataObject.SetString("sButtonLabel", ButtonLabel);
    DataObject.SetString("sIcon", ButtonIconPath);
    DataObject.SetString("sClickFunc", ExtInterfaceClickFunc);
    DataObject.SetString("sClickArg", ExtInterfaceClickArg);
    DataObject.SetBool("bFocused", bFocused);
    DataObject.SetBool("bTabEnabled", bTabEnabled);
    DataObject.SetBool("bColorIcon", bColorIcon);
    DataObject.SetBool("bDisabled", bDisabled);
    as_setDialogButton(ButtonIndex, DataObject);
    //return;    
}

protected function as_setDialogButton(float ButtonIndex, GFxObject Object)
{
    local GFxObject BoundObject;

    BoundObject = GetBoundObject();
    // End:0x53
    if(BoundObject != none)
    {
        BoundObject.ActionScriptVoid("basicButtonData");
    }
    //return;    
}

function as_setConfirmFocus()
{
    local GFxObject BoundObject;

    BoundObject = GetBoundObject();
    // End:0x50
    if(BoundObject != none)
    {
        BoundObject.ActionScriptVoid("focusConfirm");
    }
    //return;    
}

protected function string as_addDialogInputBox(int Index, string HeaderLabel, int MaxChars, string InputFunc, bool bHasPassword, bool bDisableIME, string sDefaultString)
{
    local GFxObject BoundObject;

    BoundObject = GetBoundObject();
    // End:0x56
    if(BoundObject != none)
    {
        return BoundObject.ActionScriptString("addDialogInputBox");
    }
    return "";
    //return ReturnValue;    
}

protected function string AddInputBox(optional int Index, optional string HeaderLabel, optional int MaxInputChars, optional string InputFunctionName, optional bool bDisableIME, optional bool bHasPassword, optional string sDefaultText)
{
    MaxInputChars = 48;
    InputFunctionName = "ei_InputString";
    bDisableIME = false;
    bHasPassword = false;
    sDefaultText = "";
    return as_addDialogInputBox(Index, HeaderLabel, MaxInputChars, InputFunctionName, bHasPassword, bDisableIME, sDefaultText);
    //return ReturnValue;    
}

protected function as_addDialogBoxItem()
{
    local GFxObject BoundObject;

    BoundObject = GetBoundObject();
    // End:0x54
    if(BoundObject != none)
    {
        BoundObject.ActionScriptVoid("addDialogBoxItem");
    }
    //return;    
}

protected function as_updateDialogBoxItem(const BaseInventoryEntry ItemEntry)
{
    local GFxObject BoundObject;

    BoundObject = GetBoundObject();
    // End:0x57
    if(BoundObject != none)
    {
        BoundObject.ActionScriptVoid("updateDialogBoxItem");
    }
    //return;    
}

protected function AddItemTileFromID(int UnlockID)
{
    local BaseInventoryEntry ItemEntry;

    // End:0x80
    if(Outer.GetItemTileFromCombinedItem(ItemEntry, UnlockID, true, true))
    {
        as_addDialogBoxItem();
        Outer.Advance(0.0);
        as_updateDialogBoxItem(ItemEntry);
    }
    //return;    
}

protected function AddItemTileFromInventoryItem(const InventoryMetaData InventoryItem)
{
    local BaseInventoryEntry ItemEntry;

    // End:0x9B
    if(Outer.GetItemTileFromCombinedItem(ItemEntry, InventoryItem.ItemId, true, false,, InventoryItem))
    {
        as_addDialogBoxItem();
        Outer.Advance(0.0);
        as_updateDialogBoxItem(ItemEntry);
    }
    //return;    
}

function SetHeaderText(string LocHeaderText)
{
    local GFxObject BoundObject;

    BoundObject = GetBoundObject();
    // End:0x54
    if(BoundObject != none)
    {
        BoundObject.SetString("header", LocHeaderText);
    }
    //return;    
}

function SetBodyText(string LocBodyText)
{
    local GFxObject BoundObject;

    BoundObject = GetBoundObject();
    // End:0x56
    if(BoundObject != none)
    {
        BoundObject.SetString("bodyText", LocBodyText);
    }
    //return;    
}

function SetShareCode(string LocShareCode)
{
    local GFxObject BoundObject;

    BoundObject = GetBoundObject();
    // End:0x57
    if(BoundObject != none)
    {
        BoundObject.SetString("shareCode", LocShareCode);
    }
    //return;    
}

// Export UFoxDialogBoxBase::execInsertIntToString(FFrame&, void* const)
native function string InsertIntToString(string BaseString, int IntToInsert);

// Export UFoxDialogBoxBase::execInsertStringToString(FFrame&, void* const)
native function string InsertStringToString(string BaseString, string StringToInsert);

function ei_Play_ChanceBox_ItemReveal()
{
    Outer.PCOwner.PostAkEvent(FanFare_ItemReveal_Audio);
    //return;    
}

function int GetCurrencyAmount(Engine.OnlineSubsystemTypes.ECurrency CurrencyType)
{
    local FoxDataStore_StoreData StoreDataStore;

    StoreDataStore = FoxDataStore_StoreData(class'UIInteraction'.static.GetDataStoreClient().FindDataStore('FoxStoreData'));
    // End:0xC5
    if(StoreDataStore != none)
    {
        switch(CurrencyType)
        {
            // End:0x9B
            case 0:
                return StoreDataStore.CachedGP;
                // End:0xC5
                break;
            // End:0xC2
            case 1:
                return StoreDataStore.CachedZP;
                // End:0xC5
                break;
            // End:0xFFFF
            default:
            }
            return -1;
            //return ReturnValue;            
}

event SetGPAndZP(int GPAmount, int ZPAmount, optional bool bGPAmountValid, optional bool bZPAmountValid)
{
    local GFxObject BoundObject;

    bGPAmountValid = true;
    bZPAmountValid = true;
    BoundObject = GetBoundObject();
    // End:0x112
    if(BoundObject != none)
    {
        // End:0x9F
        if(GPAmount != CachedGPAmount)
        {
            CachedGPAmount = GPAmount;
            BoundObject.SetInt("gpAmount", GPAmount);
            as_showUpdatingGPWallet(!bGPAmountValid);
        }
        // End:0x112
        if(ZPAmount != CachedZPAmount)
        {
            CachedZPAmount = ZPAmount;
            BoundObject.SetInt("zenAmount", ZPAmount);
            as_showUpdatingPremiumWallet(!bZPAmountValid);
        }
    }
    //return;    
}

function as_showUpdatingPremiumWallet(bool bShow)
{
    local GFxObject BoundObject;

    BoundObject = GetBoundObject();
    // End:0x5D
    if(BoundObject != none)
    {
        BoundObject.ActionScriptVoid("showUpdatingPremiumWallet");
    }
    //return;    
}

function as_showUpdatingGPWallet(bool bShow)
{
    local GFxObject BoundObject;

    BoundObject = GetBoundObject();
    // End:0x58
    if(BoundObject != none)
    {
        BoundObject.ActionScriptVoid("showUpdatingGPWallet");
    }
    //return;    
}

defaultproperties
{
    AcceptIcon="img://Menu.t_continueIcon"
    CancelIcon="img://Menu.t_arrowLeft"
    AcceptButtonIDPostFix="-accept"
    CancelButtonIDPostFix="-cancel"
    MiscButtonIDPostFix="-misc"
    DefaultValidVerifyTagColor="0x00CC88"
    DefaultInvalidVerifyTagColor="0xCC0000"
    OpenDialogEvent=AkEvent'A_Menu.UI_2.Play_UI_FX_001'
    LocAcceptLabel="ACCEPT"
    LocAcceptLabel_X="{XboxTypeS_A,18,18,-10}ACCEPT"
    LocStringConfirm="CONFIRM"
    LocCancelLabel="CANCEL"
    LocCancelLabel_B="{XboxTypeS_B,18,18,-10}CANCEL"
    LocVerifyLabel="VERIFY"
    CurrencyColors(0)="0x4dd3f6"
    CurrencyColors(1)="0x6ccf67"
    CachedGPAmount=-99999999
    CachedZPAmount=-99999999
    FanFare_ItemReveal_Audio=AkEvent'A_Menu.ChancePacks.Play_UI_ChancePack_ItemReveal'
}