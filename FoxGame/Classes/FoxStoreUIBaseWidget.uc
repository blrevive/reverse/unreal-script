/*******************************************************************************
 * FoxStoreUIBaseWidget generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxStoreUIBaseWidget extends Object within FoxStoreUIExtended
    abstract
    native(UI);

var private GFxObject WidgetObject;

protected function BindObject(string InObjectPath)
{
    WidgetObject = Outer.Outer.Outer.GetVariableObject(InObjectPath);
    //return;    
}

protected function UnbindObject()
{
    WidgetObject = none;
    //return;    
}

function GFxObject GetBoundObject()
{
    return WidgetObject;
    //return ReturnValue;    
}
