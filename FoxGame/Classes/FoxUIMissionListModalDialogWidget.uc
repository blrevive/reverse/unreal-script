/*******************************************************************************
 * FoxUIMissionListModalDialogWidget generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxUIMissionListModalDialogWidget extends FoxUIModalDialogWidget within GFxMoviePlayer;

defaultproperties
{
    WidgetContainerPath="_root.missionList"
}