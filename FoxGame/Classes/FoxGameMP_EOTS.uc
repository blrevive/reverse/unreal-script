/*******************************************************************************
 * FoxGameMP_EOTS generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxGameMP_EOTS extends FoxGameMP_CP
    config(Game)
    hidecategories(Navigation,Movement,Collision);

var const config int TeamIntervalScoreNodesCaptured1;
var const config int TeamIntervalScoreNodesCaptured2;
var const config int TeamIntervalScoreNodesCaptured3;
var const config int TeamIntervalScoreNodesCaptured4;
var const config int PointsFlagCapturePerNodesCaptured1;
var const config int PointsFlagCapturePerNodesCaptured2;
var const config int PointsFlagCapturePerNodesCaptured3;
var const config int PointsFlagCapturePerNodesCaptured4;
var const config int TeamLosingRespawnTime;
var const config int LosingTeamCatchUpPoints;
var const config float BecomeLosingTeamRatio;
var int LosingTeamIndex;
var transient FoxGameObject_EOTSFlag FlagObject;

function NotifyFlagSpawned(FoxGameObject_EOTSFlag NewFlag)
{
    FlagObject = NewFlag;
    //return;    
}

function UpdatePointCaptureScore()
{
    local int I, J;
    local FoxGameObject_ControlPoint CP;
    local TeamInfo OwningTeam;
    local float DT;

    DT = WorldInfo.TimeSeconds - LastTickTime;
    // End:0x46
    if(Teams.Length < 2)
    {
        return;
    }
    Teams[0].Score += (float(GetScoreIntervalForPointsControlled(GetNumberOfPointsControlledByTeam(0))) * DT);
    Teams[1].Score += (float(GetScoreIntervalForPointsControlled(GetNumberOfPointsControlledByTeam(1))) * DT);
    UpdateLosingTeam();
    I = 0;
    J0xE3:
    // End:0x29F [Loop If]
    if(I < ControlPoints.Length)
    {
        CP = ControlPoints[I];
        // End:0x291
        if(CP != none)
        {
            OwningTeam = CP.GetOwningTeam();
            // End:0x291
            if((OwningTeam != none) && OwningTeam.Score >= float(GoalScore))
            {
                OwningTeam.Score = float(GoalScore);
                J = 0;
                J0x1C4:
                // End:0x284 [Loop If]
                if(J < ControlPoints.Length)
                {
                    // End:0x276
                    if((ControlPoints[J] != none) && ControlPoints[J].CurrentlyUsingPawn != none)
                    {
                        ControlPoints[J].CancelMinigame(ControlPoints[J].CurrentlyUsingPawn);
                    }
                    ++ J;
                    // [Loop Continue]
                    goto J0x1C4;
                }
                EndRound();
                // [Explicit Break]
                goto J0x29F;
            }
        }
        ++ I;
        J0x29F:
        // [Loop Continue]
        goto J0xE3;
    }
    //return;    
}

function int GetNumberOfPointsControlledByTeam(int TeamIndex)
{
    local FoxGameObject_ControlPoint CP;
    local TeamInfo OwningTeam;
    local int I, NumNodesControlled;

    I = 0;
    J0x0B:
    // End:0xCE [Loop If]
    if(I < ControlPoints.Length)
    {
        CP = ControlPoints[I];
        // End:0xC0
        if(CP != none)
        {
            OwningTeam = CP.GetOwningTeam();
            // End:0xC0
            if((OwningTeam != none) && OwningTeam.TeamIndex == TeamIndex)
            {
                ++ NumNodesControlled;
            }
        }
        ++ I;
        // [Loop Continue]
        goto J0x0B;
    }
    return NumNodesControlled;
    //return ReturnValue;    
}

function int GetScoreIntervalForPointsControlled(int PointsControlled)
{
    switch(PointsControlled)
    {
        // End:0x24
        case 1:
            return TeamIntervalScoreNodesCaptured1;
            // End:0x60
            break;
        // End:0x36
        case 2:
            return TeamIntervalScoreNodesCaptured2;
            // End:0x60
            break;
        // End:0x48
        case 3:
            return TeamIntervalScoreNodesCaptured3;
            // End:0x60
            break;
        // End:0x5A
        case 4:
            return TeamIntervalScoreNodesCaptured4;
            // End:0x60
            break;
        // End:0xFFFF
        default:
            // End:0x60
            break;
    }
    return 0;
    //return ReturnValue;    
}

function FlagCaptured(FoxPRI CapturingPRI)
{
    local int I;
    local FoxPRI FPRI;

    // End:0x35C
    if(CapturingPRI != none)
    {
        CapturingPRI.Team.Score += float(GetFlagCaptureScoreForPointsControlled(GetNumberOfPointsControlledByTeam(CapturingPRI.Team.TeamIndex)));
        UpdateLosingTeam();
        CapturingPRI.bForceNetUpdate = true;
        ScoreEvent(CapturingPRI, 2);
        ++ CapturingPRI.NumFlagCaptures;
        DirtyThreatFlag(CapturingPRI.Team.TeamIndex);
        BroadcastCombatUpdate(CapturingPRI, 2);
        I = 0;
        J0x14A:
        // End:0x2EF [Loop If]
        if(I < GameReplicationInfo.PRIArray.Length)
        {
            FPRI = FoxPRI(GameReplicationInfo.PRIArray[I]);
            // End:0x2E1
            if(FPRI.IsValidPlayer() && FPRI != CapturingPRI)
            {
                // End:0x26D
                if(FPRI.Team.TeamIndex == CapturingPRI.Team.TeamIndex)
                {
                    ScoreEvent(FPRI, 5);
                }
                FoxPC(FPRI.Owner).ClientUnreliableTeamScoreEvent(2, byte(CapturingPRI.Team.TeamIndex));
            }
            ++ I;
            // [Loop Continue]
            goto J0x14A;
        }
        ++ CapturingPRI.NumGameCaptures;
        // End:0x35C
        if(CapturingPRI.Team.Score >= float(GoalScore))
        {
            EndRound();
        }
    }
    //return;    
}

function int GetFlagCaptureScoreForPointsControlled(int PointsControlled)
{
    switch(PointsControlled)
    {
        // End:0x24
        case 1:
            return PointsFlagCapturePerNodesCaptured1;
            // End:0x60
            break;
        // End:0x36
        case 2:
            return PointsFlagCapturePerNodesCaptured2;
            // End:0x60
            break;
        // End:0x48
        case 3:
            return PointsFlagCapturePerNodesCaptured3;
            // End:0x60
            break;
        // End:0x5A
        case 4:
            return PointsFlagCapturePerNodesCaptured4;
            // End:0x60
            break;
        // End:0xFFFF
        default:
            // End:0x60
            break;
    }
    return 0;
    //return ReturnValue;    
}

function ScoreValidKill(Controller Killer, Controller KilledPlayer)
{
    super(FoxGame).ScoreValidKill(Killer, KilledPlayer);
    // End:0x149
    if(((KilledPlayer.Pawn != none) && FoxPawn(KilledPlayer.Pawn).CarriedObject != none) && FoxGameObject_EOTSFlag(FoxPawn(KilledPlayer.Pawn).CarriedObject) != none)
    {
        // End:0x149
        if(Killer != none)
        {
            Killer.PlayerReplicationInfo.bForceNetUpdate = true;
            ScoreEvent(FoxPRI(Killer.PlayerReplicationInfo), 3);
        }
    }
    //return;    
}

function FlagPickedUp(FoxGameObject_Flag Flag, FoxPRI FoxPRI)
{
    local FoxGameObject_EOSPoint EOSPoint;

    BroadcastPawnUseDialog(0,, byte(FoxPRI.Team.TeamIndex));
    // End:0xEE
    if(FoxPRI != none)
    {
        FoxPRI.bForceNetUpdate = true;
        // End:0xEE
        if((Flag.LastCarriedPawn == none) || Flag.LastCarriedPawn.PlayerReplicationInfo != FoxPRI)
        {
            ScoreEvent(FoxPRI, 1);
        }
    }
    // End:0x13C
    foreach WorldInfo.DynamicActors(class'FoxGameObject_EOSPoint', EOSPoint)
    {
        EOSPoint.CheckFlagCapture();        
    }    
    //return;    
}

function float GetRespawnTimeForPlayer(Controller Player)
{
    // End:0x62
    if(Player.PlayerReplicationInfo.Team.TeamIndex == LosingTeamIndex)
    {
        return float(TeamLosingRespawnTime);
    }
    return GameRespawnTime;
    //return ReturnValue;    
}

function UpdateLosingTeam()
{
    local float Team1LosingRatio, Team2LosingRatio;

    // End:0xD4
    if((Teams[0].Score - float(LosingTeamCatchUpPoints)) > Teams[1].Score)
    {
        Team2LosingRatio = ((Teams[1].Score > float(0)) ? Teams[0].Score / Teams[1].Score : BecomeLosingTeamRatio);
    }
    // End:0x1B7
    else
    {
        // End:0x1A8
        if((Teams[1].Score - float(LosingTeamCatchUpPoints)) > Teams[0].Score)
        {
            Team1LosingRatio = ((Teams[0].Score > float(0)) ? Teams[1].Score / Teams[0].Score : BecomeLosingTeamRatio);
        }
        // End:0x1B7
        else
        {
            LosingTeamIndex = -1;
        }
    }
    // End:0x211
    if(LosingTeamIndex == -1)
    {
        // End:0x1EF
        if(Team1LosingRatio >= BecomeLosingTeamRatio)
        {
            LosingTeamIndex = 0;
        }
        // End:0x211
        else
        {
            // End:0x211
            if(Team2LosingRatio >= BecomeLosingTeamRatio)
            {
                LosingTeamIndex = 1;
            }
        }
    }
    //return;    
}

function EndGame(PlayerReplicationInfo Winner, string Reason)
{
    // End:0x32
    if(FlagObject != none)
    {
        FlagObject.Destroy();
        FlagObject = none;
    }
    super.EndGame(Winner, Reason);
    //return;    
}

protected function ResetForRoundEnd()
{
    super.ResetForRoundEnd();
    // End:0x3C
    if(FlagObject != none)
    {
        FlagObject.Destroy();
        FlagObject = none;
    }
    //return;    
}

defaultproperties
{
    TeamIntervalScoreNodesCaptured1=1
    TeamIntervalScoreNodesCaptured2=2
    TeamIntervalScoreNodesCaptured3=4
    TeamIntervalScoreNodesCaptured4=8
    PointsFlagCapturePerNodesCaptured1=100
    PointsFlagCapturePerNodesCaptured2=100
    PointsFlagCapturePerNodesCaptured3=100
    PointsFlagCapturePerNodesCaptured4=100
    TeamLosingRespawnTime=7
    LosingTeamCatchUpPoints=100
    BecomeLosingTeamRatio=1.50
    LosingTeamIndex=-1
    GameTypeStatsClass=class'FoxOnlineStatsWriteGameTypeEOTS'
    GameModeContext=6
    GameTypeString="NetWar"
    GameTypeAbbreviatedName="NETWAR"
    HUDType=class'FoxHudEOTS'
    GoalScore=1600
    GameReplicationInfoClass=class'FoxGRI_EOTS'
    OnlineGameSettingsClass=class'FoxGameSettingsEOTS'
}