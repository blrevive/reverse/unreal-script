/*******************************************************************************
 * FoxTouchGesture_Swipe generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxTouchGesture_Swipe extends FoxTouchGesture
    native;

var bool bSideSpecific;
var bool bLeftSide;
var bool bVerticalMin;
var bool bVerticalMax;
var bool bHorizontalMin;
var bool bHorizontalMax;
var bool bDirectionHeuristic;
var float MinimumVSwipeAmount;
var float MaximumVSwipeAmount;
var float MinimumHSwipeAmount;
var float MaximumHSwipeAmount;
var Vector2D DirectionHeuristic;
var float AngularTolerance;
var int TauntIndex;
var int LastFinishedTouchHandle;

defaultproperties
{
    MinimumVSwipeAmount=0.30
    MaximumHSwipeAmount=0.20
    LastFinishedTouchHandle=-1
    RequiredTouchCount=1
}