/*******************************************************************************
 * FoxWeapon_SingleActionMeleeBase generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxWeapon_SingleActionMeleeBase extends FoxWeapon_MeleeBase
    config(Knife)
    hidecategories(Navigation);

var bool bNeedsCock;
var bool bDoLunge;
var(Animation) const WeaponAnimSet AN_HitMelee;

replication
{
    // Pos:0x000
    if(bNetOwner && Role == ROLE_Authority)
        bNeedsCock
}

simulated function float StartFPMeleeAnim(bool bHitSomething)
{
    bDoLunge = bHitSomething;
    return super(FoxWeapon).StartFPMeleeAnim(bHitSomething);
    //return ReturnValue;    
}

simulated function float StartMeleeAnim(optional float PrecentOfAnimLength)
{
    local float MeleeTime;

    PrecentOfAnimLength = 1.0;
    // End:0x67
    if(bDoLunge)
    {
        MeleeTime = PlayWeaponSet(AN_HitMelee, 1.0, false, true);
        SetTimer(MeleeTime * PrecentOfAnimLength, false, 'StopMeleeAnim');
        return MeleeTime;
    }
    return super(FoxWeapon).StartMeleeAnim(PrecentOfAnimLength);
    //return ReturnValue;    
}

simulated function ProcessInstantHit(byte FiringMode, ImpactInfo Impact, optional int NumHits)
{
    bNeedsCock = Impact.HitActor != none;
    super(FoxWeapon).ProcessInstantHit(FiringMode, Impact, NumHits);
    //return;    
}

simulated function bool HasAmmo(byte FireModeNum, optional int Amount)
{
    // End:0x12
    if(!bConsumeAmmo)
    {
        return true;
    }
    // End:0x58
    if(((FireModeNum == 0) || FireModeNum == 2) || FireModeNum == 3)
    {
        return !bNeedsCock;
    }
    return true;
    //return ReturnValue;    
}

simulated function bool HasAnyAmmo()
{
    return true;
    //return ReturnValue;    
}

simulated function bool CanReload()
{
    local bool bInstigatorCanReload;
    local FoxPawn P;

    P = FoxPawn(Instigator);
    bInstigatorCanReload = (P != none) && P.CanReloadWeapon();
    return bInstigatorCanReload && bNeedsCock;
    //return ReturnValue;    
}

simulated function bool ShouldAutoReload()
{
    return true;
    //return ReturnValue;    
}

simulated function PerformReload(const int AmountToReload)
{
    bNeedsCock = false;
    //return;    
}

simulated function bool CanMelee()
{
    return !bNeedsCock && super(FoxWeapon).CanMelee();
    //return ReturnValue;    
}

simulated state Melee
{
    simulated function StopMeleeAnim()
    {
        // End:0x9A
        if(bDoLunge)
        {
            FoxPawn(Instigator).StopFirstAndThirdPersonAnim(AN_HitMelee.FP_Anim, AN_HitMelee.TP_Anim, AN_HitMelee.AbortBlendOutTime);
            GotoState('Active');
        }
        // End:0xA4
        else
        {
            super.StopMeleeAnim();
        }
        //return;        
    }
    stop;    
}

defaultproperties
{
    AN_HitMelee=(TP_Anim=(AnimName=(None,None,Melee_B)),FP_Anim=Melee_B,WP_Anim=Melee_A,HG_Anim=None,RateMultiplier=1.0,BlendInTime=0.10,BlendOutTime=0.250,AbortBlendOutTime=0.10)
    begin object name=GearFXParticleSystemComponent0 class=FoxFirstPersonParticleSystemComponent
        ReplacementPrimitive=none
    object end
    // Reference: FoxFirstPersonParticleSystemComponent'Default__FoxWeapon_SingleActionMeleeBase.GearFXParticleSystemComponent0'
    GearFXComponent=GearFXParticleSystemComponent0
    FireFeedbackWaveform=ForceFeedbackWaveform'Default__FoxWeapon_SingleActionMeleeBase.ForceFeedbackWaveformFire'
    TightAimCameraShake=CameraShake'Default__FoxWeapon_SingleActionMeleeBase.Shake0'
    NormalCameraShake=CameraShake'Default__FoxWeapon_SingleActionMeleeBase.Shake1'
    MuzzleFlashLight=PointLightComponent'Default__FoxWeapon_SingleActionMeleeBase.LightComponent0'
    begin object name=LensComp class=LensFlareComponent
        NextTraceTime=-0.33829770
        ReplacementPrimitive=none
    object end
    // Reference: LensFlareComponent'Default__FoxWeapon_SingleActionMeleeBase.LensComp'
    MuzzleLensFlare=LensComp
    AN_FullReload=(TP_Anim=(AnimName=(None,None,Cock_A)),FP_Anim=Cock_A,WP_Anim=Cock_A)
    FiringStatesArray=/* Array type was not detected. */
    begin object name=WeaponMesh class=FoxFirstPersonSkeletalMeshComponent
        bForceRefpose=0
        bNoSkeletonUpdate=false
        ReplacementPrimitive=none
        TickGroup=ETickingGroup.TG_PreAsyncWork
    object end
    // Reference: FoxFirstPersonSkeletalMeshComponent'Default__FoxWeapon_SingleActionMeleeBase.WeaponMesh'
    Mesh=WeaponMesh
}