/*******************************************************************************
 * FoxWeapon_TargetBase generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxWeapon_TargetBase extends FoxWeapon
    abstract
    native(Weapon)
    config(Weapon)
    hidecategories(Navigation);

var transient Actor WeaponTarget;
var const float RechargeTime;
var const bool bCanHurtMachines;
var const bool bCanHelpMachines;
var const bool bCanHelpFleshies;
var const bool bCanHurtFleshies;
var const bool bCanHelpInstigator;
var const string HelpString;
var const string HurtString;

simulated function string GetUseText()
{
    // End:0x1D
    if((ShouldHelpWeaponTarget()) == true)
    {
        return HelpString;
    }
    // End:0x37
    else
    {
        // End:0x37
        if((ShouldHurtWeaponTarget()) == true)
        {
            return HurtString;
        }
    }
    return "";
    //return ReturnValue;    
}

simulated function string GetUseIcon()
{
    return "";
    //return ReturnValue;    
}

simulated function float GetUseTime()
{
    return GetFireInterval(0);
    //return ReturnValue;    
}

simulated function bool HasSpareAmmo()
{
    return true;
    //return ReturnValue;    
}

simulated function bool ShouldRefire()
{
    // End:0x58
    if((Instigator != none) && Instigator.IsLocallyControlled())
    {
        // End:0x58
        if((IsWeaponTargetValid()) == false)
        {
            StopFire(CurrentFireMode);
            return false;
        }
    }
    return super.ShouldRefire();
    //return ReturnValue;    
}

simulated function ConsumeAmmo(byte FireModeNum)
{
    // End:0x7B
    if(bConsumeAmmo)
    {
        AmmoUsedCount = int(GetMagazineSize());
        SimulatedAmmoUsedCount = int(GetMagazineSize());
        AmmoLeftCount = 0;
        // End:0x7B
        if(Role == ROLE_Authority)
        {
            SetTimer(RechargeTime / (GetMagazineSize()), true, 'TimerAddAmmo');
        }
    }
    //return;    
}

function TimerAddAmmo()
{
    ++ AmmoLeftCount;
    -- SimulatedAmmoUsedCount;
    -- AmmoUsedCount;
    // End:0x70
    if(AmmoUsedCount <= 0)
    {
        ClearTimer('TimerAddAmmo');
        AmmoUsedCount = 0;
        AmmoLeftCount = int(GetMagazineSize());
        SimulatedAmmoUsedCount = 0;
    }
    //return;    
}

simulated event int GetAmmoUsedCount()
{
    return AmmoUsedCount;
    //return ReturnValue;    
}

// Export UFoxWeapon_TargetBase::execIsWeaponTargetValid(FFrame&, void* const)
native simulated function bool IsWeaponTargetValid();

simulated event bool ShouldHelpWeaponTarget()
{
    // End:0x1A
    if(FoxPawn(WeaponTarget) == none)
    {
        return false;
    }
    // End:0xDA
    if((FoxPawn(WeaponTarget).bIsMachine == bCanHelpMachines) || !FoxPawn(WeaponTarget).bIsMachine == bCanHelpFleshies)
    {
        return Instigator.IsValidEnemyTargetFor(Pawn(WeaponTarget).PlayerReplicationInfo, false) == false;
    }
    // End:0xDC
    else
    {
        return false;
    }
    //return ReturnValue;    
}

simulated event bool ShouldHurtWeaponTarget()
{
    // End:0x1A
    if(FoxPawn(WeaponTarget) == none)
    {
        return false;
    }
    // End:0xDA
    if((FoxPawn(WeaponTarget).bIsMachine == bCanHurtMachines) || !FoxPawn(WeaponTarget).bIsMachine == bCanHurtFleshies)
    {
        return Instigator.IsValidEnemyTargetFor(Pawn(WeaponTarget).PlayerReplicationInfo, false) == true;
    }
    // End:0xDC
    else
    {
        return false;
    }
    //return ReturnValue;    
}

defaultproperties
{
    FireFeedbackWaveform=ForceFeedbackWaveform'Default__FoxWeapon_TargetBase.ForceFeedbackWaveformFire'
    TightAimCameraShake=CameraShake'Default__FoxWeapon_TargetBase.Shake0'
    NormalCameraShake=CameraShake'Default__FoxWeapon_TargetBase.Shake1'
    MuzzleFlashLight=PointLightComponent'Default__FoxWeapon_TargetBase.LightComponent0'
    begin object name=LensComp class=LensFlareComponent
        NextTraceTime=-0.37788020
        ReplacementPrimitive=none
    object end
    // Reference: LensFlareComponent'Default__FoxWeapon_TargetBase.LensComp'
    MuzzleLensFlare=LensComp
    begin object name=WeaponMesh class=FoxFirstPersonSkeletalMeshComponent
        ReplacementPrimitive=none
    object end
    // Reference: FoxFirstPersonSkeletalMeshComponent'Default__FoxWeapon_TargetBase.WeaponMesh'
    Mesh=WeaponMesh
}