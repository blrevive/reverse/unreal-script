/*******************************************************************************
 * FoxCreatePlayerUI generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxCreatePlayerUI extends FoxUIBaseTask within FoxUI
    config(UI);

var config int ProfileSyncInterval;
var config string TrainingButtonID;
var private string PlayerName;
var private Engine.OnlineSubsystemTypes.EPlayerGender PlayerGender;
var private string PostCommandGroup;
var private const localized string LocErrorCreatePlayerFailed;
var private const localized string LocCreatingPlayer;
var private const localized string LocCreatePlayerComplete;
var private const localized string LocCreatePlayerSyncing;
var delegate<OnPostCreatePlayer> __OnPostCreatePlayer__Delegate;

delegate OnPostCreatePlayer(bool bComplete, string CommandGroup)
{
    //return;    
}

function bool CreatePlayer(const string NewPlayerName, Engine.OnlineSubsystemTypes.EPlayerGender NewPlayerGender)
{
    // End:0x40
    if(StartTask())
    {
        PlayerName = NewPlayerName;
        PlayerGender = NewPlayerGender;
        ConfirmCreateNewPlayerIntent(true);
        return true;
    }
    return false;
    //return ReturnValue;    
}

function CancelCreatePlayer()
{
    NotifyProcessComplete(1);
    //return;    
}

protected function RegisterOnlineDelegates()
{
    Outer.PCOwner.OnlineSub.StoreInterface.AddCharacterCreatedCompleteDelegate(0, CreatePlayerComplete);
    //return;    
}

protected function CleanupOnlineDelegates()
{
    Outer.PCOwner.OnlineSub.StoreInterface.ClearCharacterCreatedCompleteDelegate(0, CreatePlayerComplete);
    //return;    
}

protected function NotifyProcessComplete(FoxDialogBoxBase.EDialogBoxConfirmType Selection)
{
    super.NotifyProcessComplete(Selection);
    Outer.CloseDialogBox();
    Outer.OnlineSub.PlayerInterfaceEx.AddInGamePost(8);
    OnPostCreatePlayer(Selection == 0, PostCommandGroup);
    //return;    
}

protected function NotifyProcessFailed(FoxDialogBoxBase.EDialogBoxConfirmType Selection)
{
    NotifyProcessComplete(1);
    //return;    
}

private final function ConfirmCreateNewPlayerIntent(bool bResult)
{
    local DialogBoxProperties Properties;
    local array<int> ProfileFlags;

    // End:0x1B5
    if(bResult)
    {
        Properties.DialogType = 0;
        Properties.TitleText = Outer.LocWorkingLabel;
        Properties.BodyText = LocCreatingPlayer;
        Properties.bShowTimer = true;
        Outer.ShowDialogBox(Properties);
        ProfileFlags.Length = 2;
        ProfileFlags[0] = 0;
        switch(PlayerGender)
        {
            // End:0x116
            case 0:
                ProfileFlags[1] = 1;
                // End:0x12F
                break;
            // End:0x12C
            case 1:
                ProfileFlags[1] = 2;
                // End:0x12F
                break;
            // End:0xFFFF
            default:
                // End:0x1B2
                if(!Outer.PCOwner.OnlineSub.StoreInterface.CreateCharacter(PlayerName, ProfileFlags))
                {
                    CreatePlayerComplete(0, false);
                }
                // End:0x1C1
                break;
            }
    }
    NotifyProcessComplete(1);
    //return;    
}

private final function CreatePlayerComplete(byte LocalUserNum, bool bResult)
{
    local DialogBoxProperties Properties;
    local ProfileEmblemInfo EmblemInfo;

    // End:0xC4
    if(bResult)
    {
        // End:0x97
        if(Outer.PCOwner != none)
        {
            class'FoxLoadoutInfo'.static.SetEmblemToDefaults(EmblemInfo);
            Outer.PCOwner.SetEmblem(EmblemInfo);
        }
        Outer.CloseDialogBox();
        ConfirmTrainingLevelIntent(true);
    }
    // End:0x19A
    else
    {
        Properties.DialogType = 1;
        Properties.TitleText = Outer.LocFailureLabel;
        Properties.BodyText = LocErrorCreatePlayerFailed;
        Properties.OnDialogButtonPressed = NotifyProcessFailed;
        Outer.ShowDialogBox(Properties);
    }
    //return;    
}

private final function ConfirmTrainingLevelIntent(bool bResult)
{
    PostCommandGroup = ((bResult) ? "trainingLevel" : "");
    NotifyProcessComplete(0);
    //return;    
}

defaultproperties
{
    ProfileSyncInterval=25
    TrainingButtonID="training-create-char"
    LocErrorCreatePlayerFailed="Registration failed. Please try again later."
    LocCreatingPlayer="Registering agent..."
    LocCreatePlayerComplete="Registration complete. Would you like to proceed to training? If not, this can be accessed later from the Main Menu."
    LocCreatePlayerSyncing="Syncing character across regions..."
}