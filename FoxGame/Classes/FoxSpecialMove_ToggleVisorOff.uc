/*******************************************************************************
 * FoxSpecialMove_ToggleVisorOff generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxSpecialMove_ToggleVisorOff extends FoxSpecialMove_ToggleVisor;

protected function bool InternalCanDoSpecialMove()
{
    return !PawnOwner.IsDoingSpecialMove(16);
    //return ReturnValue;    
}

simulated function StartSpecialMove()
{
    local FoxPC PC;

    super(FoxSpecialMove_AnimationBase).StartSpecialMove();
    PC = FoxPC(PawnOwner.Controller);
    // End:0x94
    if(PC != none)
    {
        PC.ClearTimer('VisorRampTimer');
        PC.HRVState = 4;
    }
    //return;    
}

simulated function CleanUpSpecialMove()
{
    super(FoxSpecialMove_AnimationBase).CleanUpSpecialMove();
    PawnOwner.OnVisorToggled(false);
    //return;    
}

simulated function bool RequestEndSpecialMove(FoxTypes.ESpecialMove NextMove)
{
    // End:0x52
    if(PawnOwner.IsLocallyControlled() && PawnOwner.IsTimerActive('SpecialMoveTimerFinished'))
    {
        return false;
    }
    return super(FoxSpecialMove).RequestEndSpecialMove(NextMove);
    //return ReturnValue;    
}

defaultproperties
{
    Anims(0)=(TP_Anim=(AnimName=(None,None,HRV_A)),FP_Anim=VisorDown_a,WP_Anim=None,HG_Anim=None,RateMultiplier=1.0,BlendInTime=0.250,BlendOutTime=0.250,AbortBlendOutTime=0.10)
}