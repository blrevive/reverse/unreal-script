/*******************************************************************************
 * FoxGameObject_Carry generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxGameObject_Carry extends FoxGameObject
    abstract
    native
    notplaceable
    hidecategories(Navigation);

var repnotify transient FoxPawn CarriedByPawn;
var transient FoxPawn LastCarriedPawn;
var name AttachSocketName;
var transient bool bCarried;
var const bool bCanSprint;
var const float MovementMultiplier;
var Material ObjectMaterial;
var transient MaterialInstanceConstant ObjectMaterialInstance;
var MaterialInstanceConstant RedMIC;
var MaterialInstanceConstant BlueMIC;
var repnotify transient LinearColor ObjectColor;
var export editinline PointLightComponent ObjectLight;
var export editinline CylinderComponent ObjectCylinder;
var transient Vector SpawnLocation;
var float DroppedObjectReturnTime;

replication
{
    // Pos:0x000
    if(bNetDirty && Role == ROLE_Authority)
        CarriedByPawn, ObjectColor

    // Pos:0x020
    if(bNetInitial && Role == ROLE_Authority)
        SpawnLocation
}

simulated event ReplicatedEvent(name VarName)
{
    // End:0x49
    if(VarName == 'CarriedByPawn')
    {
        // End:0x3C
        if(CarriedByPawn != none)
        {
            Pickup(CarriedByPawn);
        }
        // End:0x46
        else
        {
            Drop();
        }
    }
    // End:0x80
    else
    {
        // End:0x6D
        if(VarName == 'UsableByTeam')
        {
            SetEmissives();
        }
        // End:0x80
        else
        {
            super.ReplicatedEvent(VarName);
        }
    }
    //return;    
}

simulated event PostBeginPlay()
{
    local Actor TheOther;
    local FoxPC PC;

    bScriptInitialized = true;
    // End:0x95
    foreach VisibleCollidingActors(class'Actor', TheOther, ObjectCylinder.CollisionRadius)
    {
        Touch(TheOther, TheOther.CollisionComponent, vect(0.0, 0.0, 0.0), vect(0.0, 0.0, 0.0));        
    }    
    // End:0x10F
    foreach LocalPlayerControllers(class'FoxPC', PC)
    {
        // End:0x10E
        if(PC.myHUD != none)
        {
            PC.myHUD.AddPostRenderedActor(self);
        }        
    }    
    super.PostBeginPlay();
    //return;    
}

event Touch(Actor Other, PrimitiveComponent OtherComp, Vector HitLocation, Vector HitNormal)
{
    local FoxPawn TouchedPawn;

    super(Actor).Touch(Other, OtherComp, HitLocation, HitNormal);
    TouchedPawn = FoxPawn(Other);
    // End:0xBE
    if((((bScriptInitialized && TouchedPawn != none) && CanInteractWith(TouchedPawn)) && CarriedByPawn == none) && !InTeamSpawn(TouchedPawn))
    {
        Pickup(TouchedPawn);
    }
    //return;    
}

simulated function bool InTeamSpawn(FoxPawn PawnToCheck)
{
    local int I;

    I = 0;
    J0x0B:
    // End:0x7F [Loop If]
    if(I < PawnToCheck.Touching.Length)
    {
        // End:0x71
        if(FoxTeamSpawnVolume(PawnToCheck.Touching[I]) != none)
        {
            return true;
        }
        ++ I;
        // [Loop Continue]
        goto J0x0B;
    }
    return false;
    //return ReturnValue;    
}

// Export UFoxGameObject_Carry::execIsPickupTouchPossible(FFrame&, void* const)
native simulated function bool IsPickupTouchPossible(FoxPawn PlayerTouched);

simulated function Pickup(FoxPawn PickedUpPawn)
{
    local FoxPC FPC;

    // End:0x269
    if(!bCarried && PickedUpPawn != none)
    {
        bCarried = true;
        CarriedByPawn = PickedUpPawn;
        CarriedByPawn.CarriedObject = self;
        FPC = FoxPC(CarriedByPawn.Controller);
        // End:0xD0
        if((FPC != none) && !bCanSprint)
        {
            FPC.LocalSetSprint(false);
        }
        SetPhysics(0);
        SetBase(CarriedByPawn,, CarriedByPawn.Mesh, AttachSocketName);
        ObjectMesh.SetShadowParent(CarriedByPawn.Mesh);
        ObjectMesh.SetLightEnvironment(CarriedByPawn.Mesh.LightEnvironment);
        ObjectMesh.SetActorCollision(false, false);
        // End:0x1FC
        if(CarriedByPawn.IsFirstPerson())
        {
            ObjectMesh.SetHidden(true);
        }
        ObjectCylinder.SetCylinderSize(0.0, 0.0);
        ObjectCylinder.SetActorCollision(true, false, false);
        CollisionComponent = ObjectMesh;
        bNetDirty = true;
        J0x269:
    }
    // End:0x269
    else
    {
    }
    //return;    
}

simulated function Drop()
{
    local Vector NewLoc;
    local Rotator NewRot;

    // End:0x1EF
    if(bCarried)
    {
        SetBase(none);
        // End:0x16A
        if(CarriedByPawn != none)
        {
            // End:0x4A
            if(Role == ROLE_Authority)
            {
                LastCarriedPawn = CarriedByPawn;
            }
            CarriedByPawn.Mesh.GetSocketWorldLocationAndRotation(AttachSocketName, NewLoc, NewRot);
            NewLoc = CarriedByPawn.Location;
            NewRot = rot(0, 0, 0);
            NewRot.Yaw = CarriedByPawn.Rotation.Yaw;
            SetLocation(NewLoc);
            SetRotation(NewRot);
            CarriedByPawn.CarriedObject = none;
            CarriedByPawn = none;
        }
        ObjectMesh.SetHidden(false);
        ObjectMesh.SetShadowParent(none);
        ObjectMesh.SetActorCollision(true, true);
        SetCollision(true, true);
        SetPhysics(2);
        bCarried = false;
        bNetDirty = true;
    }
    //return;    
}

simulated event Destroyed()
{
    local PlayerController PC;

    // End:0x79
    foreach LocalPlayerControllers(class'PlayerController', PC)
    {
        // End:0x78
        if(PC.myHUD != none)
        {
            PC.myHUD.RemovePostRenderedActor(self);
        }        
    }    
    // End:0x93
    if(CarriedByPawn != none)
    {
        Drop();
    }
    ObjectMesh.SetHidden(true);
    SetHidden(true);
    super.Destroyed();
    //return;    
}

simulated event Landed(Vector HitNormal, Actor FloorActor)
{
    super(Actor).Landed(HitNormal, FloorActor);
    SetPhysics(5);
    ObjectCylinder.SetCylinderSize(ObjectCylinder.default.CollisionRadius, ObjectCylinder.default.CollisionHeight);
    ObjectCylinder.SetActorCollision(true, false, false);
    SetCollision(true, false);
    CollisionComponent = ObjectCylinder;
    //return;    
}

function ReturnToBase(optional FoxPRI ReturningPRI)
{
    Destroy();
    //return;    
}

function TakeDamage(int Damage, Controller InstigatedBy, Vector HitLocation, Vector Momentum, class<DamageType> DamageType, optional TraceHitInfo HitInfo, optional Actor DamageCauser)
{
    // End:0x24
    if(DamageType == class'FoxDamageType_RanOver')
    {
        ReturnToBase();
    }
    super(Actor).TakeDamage(Damage, InstigatedBy, HitLocation, Momentum, DamageType, HitInfo, DamageCauser);
    //return;    
}

simulated function SetEmissives()
{
    local FoxPC LocalPC;
    local int LocalTeamNum;
    local MaterialInstanceConstant MIC;

    // End:0x2B
    if(WorldInfo.NetMode == NM_DedicatedServer)
    {
        return;
    }
    // End:0x63
    if(UsableByTeam == 0)
    {
        ObjectMesh.SetMaterial(0, none);
    }
    // End:0x1B3
    else
    {
        LocalPC = FoxPC(WorldInfo.GetALocalPlayerController());
        LocalTeamNum = LocalPC.GetTeamNum();
        // End:0x177
        if((ScriptGetTeamNum() == LocalTeamNum) || (((LocalPC != none) && LocalPC.PlayerReplicationInfo != none) && LocalPC.PlayerReplicationInfo.bOnlySpectator) && ScriptGetTeamNum() == 0)
        {
            MIC = BlueMIC;
        }
        // End:0x18A
        else
        {
            MIC = RedMIC;
        }
        ObjectMesh.SetMaterial(0, MIC);
    }
    //return;    
}

defaultproperties
{
    AttachSocketName=S_Primary
    bCanSprint=true
    MovementMultiplier=1.0
    ObjectColor=(R=0.0,G=0.0,B=0.0,A=1.0)
    begin object name=NewCylinder class=CylinderComponent
        CollisionHeight=64.0
        CollisionRadius=128.0
        ReplacementPrimitive=none
        CollideActors=true
        Translation=(X=0.0,Y=0.0,Z=48.0)
    object end
    // Reference: CylinderComponent'Default__FoxGameObject_Carry.NewCylinder'
    ObjectCylinder=NewCylinder
    begin object name=NewCylinder class=CylinderComponent
        CollisionHeight=64.0
        CollisionRadius=128.0
        ReplacementPrimitive=none
        CollideActors=true
        Translation=(X=0.0,Y=0.0,Z=48.0)
    object end
    // Reference: CylinderComponent'Default__FoxGameObject_Carry.NewCylinder'
    Components(0)=NewCylinder
    Physics=EPhysics.PHYS_Rotating
    TickGroup=ETickingGroup.TG_PostAsyncWork
    bCanBeDamaged=true
    begin object name=NewCylinder class=CylinderComponent
        CollisionHeight=64.0
        CollisionRadius=128.0
        ReplacementPrimitive=none
        CollideActors=true
        Translation=(X=0.0,Y=0.0,Z=48.0)
    object end
    // Reference: CylinderComponent'Default__FoxGameObject_Carry.NewCylinder'
    CollisionComponent=NewCylinder
}