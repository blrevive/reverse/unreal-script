/*******************************************************************************
 * FoxGameSearchTDM generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxGameSearchTDM extends FoxGameSearchCommon;

defaultproperties
{
    Query=(ValueIndex=1)
    GameSettingsClass=class'FoxGameSettingsTDM'
    LocalizedSettings=/* Array type was not detected. */
    LocalizedSettingsMappings=/* Array type was not detected. */
}