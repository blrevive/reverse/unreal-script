/*******************************************************************************
 * FoxSpecialMove_DeathAnim generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxSpecialMove_DeathAnim extends FoxSpecialMove_Injured;

protected simulated function StartAnimation()
{
    super(FoxSpecialMove_AnimationBase).StartAnimation();
    PawnOwner.SetTimer(0.80 * CachedAnimTime, false, 'DeathAnimFinished');
    //return;    
}

protected simulated function array<InjureMap> GetAnimsToUse()
{
    return PawnOwner.DeathCases;
    //return ReturnValue;    
}

defaultproperties
{
    Anims(0)=(TP_Anim=(AnimName=(Death_Gib_A)),FP_Anim=None,WP_Anim=None,HG_Anim=None,RateMultiplier=1.0,BlendInTime=0.250,BlendOutTime=0.0,AbortBlendOutTime=0.10)
}