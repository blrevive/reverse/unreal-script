/*******************************************************************************
 * FoxLiveStreamingMod_OnlyHeadShots generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxLiveStreamingMod_OnlyHeadShots extends FoxLiveStreamingMod;

protected function bool IsPositive()
{
    return false;
    //return ReturnValue;    
}

protected function OnPreDamageDealtImpl(out int Damage, int DamageZone, class<DamageType> DamageType)
{
    // End:0x4E
    if((DamageZone != 1) && (class<FoxDamageType_Corrosive_Bullet>(DamageType) != none) || class<FoxDamageType_Bullet>(DamageType) != none)
    {
        Damage = 0;
    }
    //return;    
}
