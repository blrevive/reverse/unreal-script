/*******************************************************************************
 * FoxWeaponAttachment_ShellGunBase generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxWeaponAttachment_ShellGunBase extends FoxWeaponAttachment_ShotgunBase
    config(Weapon)
    notplaceable
    hidecategories(Navigation);

simulated function PlayImpactEffects(Vector HitLocation, optional bool bCheckNearMiss)
{
    local int I;
    local Vector StartTrace, EndTrace;
    local array<ImpactInfo> ImpactList;
    local ImpactInfo RealImpact;
    local class<FoxWeapon_ShotgunBase> ShotgunClass;

    bCheckNearMiss = true;
    // End:0x30
    if(WorldInfo.NetMode == NM_DedicatedServer)
    {
        return;
    }
    ShotgunClass = class<FoxWeapon_ShotgunBase>(WeaponClass);
    // End:0x71
    if(ShotgunClass == none)
    {
        super.PlayImpactEffects(HitLocation);
        return;
    }
    // End:0x163
    if(((Instigator.FiringMode != 0) || (WeaponConfig.WeaponAmmo == none) && ShotgunClass.default.WeaponFireTypes[0] != 2) || (WeaponConfig.WeaponAmmo != none) && WeaponConfig.WeaponAmmo.default.WeaponFireType != 2)
    {
        super(FoxWeaponAttachment).PlayImpactEffects(HitLocation, false);
        return;
    }
    // End:0x1D4
    if(WorldInfo.GRI.bClientSideHitDetection && Instigator.IsLocallyControlled())
    {
        super(FoxWeaponAttachment).PlayImpactEffects(HitLocation, false);
        return;
    }
    StartTrace = GetMuzzleLoc();
    // End:0x218
    if(bDebug)
    {
        DrawDebugLine(StartTrace, HitLocation, 255, 255, 255, true);
    }
    I = 0;
    J0x223:
    // End:0x381 [Loop If]
    if(I < ShotgunClass.default.FragmentsPerShell)
    {
        EndTrace = StartTrace + (vector(AddSpread(rotator(HitLocation - StartTrace), ShotgunClass.default.FragmentSpread)) * ShotgunClass.default.MaxRange);
        RealImpact = CalcWeaponFire(StartTrace, EndTrace, ImpactList);
        // End:0x337
        if(bDebug)
        {
            DrawDebugLine(StartTrace, RealImpact.HitLocation, 0, 255, 0, true);
        }
        super(FoxWeaponAttachment).PlayImpactEffects(RealImpact.HitLocation, bCheckNearMiss);
        bCheckNearMiss = false;
        ++ I;
        // [Loop Continue]
        goto J0x223;
    }
    //return;    
}

defaultproperties
{
    begin object name=AttachmentMesh class=SkeletalMeshComponent
        ReplacementPrimitive=none
    object end
    // Reference: SkeletalMeshComponent'Default__FoxWeaponAttachment_ShellGunBase.AttachmentMesh'
    Mesh=AttachmentMesh
    MuzzleFlashLight=PointLightComponent'Default__FoxWeaponAttachment_ShellGunBase.LightComponent0'
    begin object name=LensComp class=LensFlareComponent
        NextTraceTime=-0.61357460
        ReplacementPrimitive=none
    object end
    // Reference: LensFlareComponent'Default__FoxWeaponAttachment_ShellGunBase.LensComp'
    MuzzleLensFlare=LensComp
    begin object name=AttachmentMesh class=SkeletalMeshComponent
        ReplacementPrimitive=none
    object end
    // Reference: SkeletalMeshComponent'Default__FoxWeaponAttachment_ShellGunBase.AttachmentMesh'
    Components(0)=AttachmentMesh
}