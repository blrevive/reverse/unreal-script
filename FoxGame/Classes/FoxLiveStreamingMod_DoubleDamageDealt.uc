/*******************************************************************************
 * FoxLiveStreamingMod_DoubleDamageDealt generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxLiveStreamingMod_DoubleDamageDealt extends FoxLiveStreamingMod;

protected function bool IsPositive()
{
    return true;
    //return ReturnValue;    
}

protected function OnPreDamageDealtImpl(out int Damage, int DamageZone, class<DamageType> DamageType)
{
    Damage *= float(2);
    //return;    
}
