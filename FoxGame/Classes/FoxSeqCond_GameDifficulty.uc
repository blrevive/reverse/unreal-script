/*******************************************************************************
 * FoxSeqCond_GameDifficulty generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxSeqCond_GameDifficulty extends SequenceCondition
    forcescriptorder(true)
    hidecategories(Object);

event Activated()
{
    local int Difficulty;

    Difficulty = int(GetWorldInfo().Game.GameDifficulty);
    switch(Difficulty)
    {
        // End:0x65
        case 0:
            ActivateOutputLink(0);
            // End:0x8E
            break;
        // End:0x77
        case 1:
            ActivateOutputLink(1);
            // End:0x8E
            break;
        // End:0x8B
        case 2:
            ActivateOutputLink(2);
            // End:0x8E
            break;
        // End:0xFFFF
        default:
            //return;
    }    
}

defaultproperties
{
    OutputLinks(0)=(Links=none,LinkDesc="Low",bHasImpulse=false,bDisabled=false,bDisabledPIE=false,LinkedOp=none,ActivateDelay=0.0,DrawY=0,bHidden=false,bMoving=false,bClampedMax=false,bClampedMin=false,OverrideDelta=0,PIEActivationTime=0.0,bIsActivated=false)
    OutputLinks(1)=(Links=none,LinkDesc="Med",bHasImpulse=false,bDisabled=false,bDisabledPIE=false,LinkedOp=none,ActivateDelay=0.0,DrawY=0,bHidden=false,bMoving=false,bClampedMax=false,bClampedMin=false,OverrideDelta=0,PIEActivationTime=0.0,bIsActivated=false)
    OutputLinks(2)=(Links=none,LinkDesc="High",bHasImpulse=false,bDisabled=false,bDisabledPIE=false,LinkedOp=none,ActivateDelay=0.0,DrawY=0,bHidden=false,bMoving=false,bClampedMax=false,bClampedMin=false,OverrideDelta=0,PIEActivationTime=0.0,bIsActivated=false)
    ObjName="Black Ops Difficulty"
}