/*******************************************************************************
 * FoxDestructiblePlaceable generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxDestructiblePlaceable extends FoxDestructible;

defaultproperties
{
    begin object name=DrawSphere2 class=DrawSphereComponent
        ReplacementPrimitive=none
    object end
    // Reference: DrawSphereComponent'Default__FoxDestructiblePlaceable.DrawSphere2'
    DrawDamageRadiusInner=DrawSphere2
    begin object name=DrawSphere1 class=DrawSphereComponent
        ReplacementPrimitive=none
    object end
    // Reference: DrawSphereComponent'Default__FoxDestructiblePlaceable.DrawSphere1'
    DrawDamageRadiusOuter=DrawSphere1
    begin object name=StaticMeshComponent0 class=StaticMeshComponent
        ReplacementPrimitive=none
        LightEnvironment=DynamicLightEnvironmentComponent'Default__FoxDestructiblePlaceable.MyLightEnvironment'
    object end
    // Reference: StaticMeshComponent'Default__FoxDestructiblePlaceable.StaticMeshComponent0'
    StaticMeshComponent=StaticMeshComponent0
    LightEnvironment=DynamicLightEnvironmentComponent'Default__FoxDestructiblePlaceable.MyLightEnvironment'
    Components(0)=DynamicLightEnvironmentComponent'Default__FoxDestructiblePlaceable.MyLightEnvironment'
    begin object name=StaticMeshComponent0 class=StaticMeshComponent
        ReplacementPrimitive=none
        LightEnvironment=DynamicLightEnvironmentComponent'Default__FoxDestructiblePlaceable.MyLightEnvironment'
    object end
    // Reference: StaticMeshComponent'Default__FoxDestructiblePlaceable.StaticMeshComponent0'
    Components(1)=StaticMeshComponent0
    begin object name=DrawSphere2 class=DrawSphereComponent
        ReplacementPrimitive=none
    object end
    // Reference: DrawSphereComponent'Default__FoxDestructiblePlaceable.DrawSphere2'
    Components(2)=DrawSphere2
    begin object name=DrawSphere1 class=DrawSphereComponent
        ReplacementPrimitive=none
    object end
    // Reference: DrawSphereComponent'Default__FoxDestructiblePlaceable.DrawSphere1'
    Components(3)=DrawSphere1
    begin object name=StaticMeshComponent0 class=StaticMeshComponent
        ReplacementPrimitive=none
        LightEnvironment=DynamicLightEnvironmentComponent'Default__FoxDestructiblePlaceable.MyLightEnvironment'
    object end
    // Reference: StaticMeshComponent'Default__FoxDestructiblePlaceable.StaticMeshComponent0'
    CollisionComponent=StaticMeshComponent0
}