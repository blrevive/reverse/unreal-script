/*******************************************************************************
 * FoxAIAction_ChooseTarget generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2015 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class FoxAIAction_ChooseTarget extends FoxAIAction
    native(AI)
    editinlinenew
    hidecategories(Object);

var() TargetingInfo TargetingInfo;

// Export UFoxAIAction_ChooseTarget::execInitialize(FFrame&, void* const)
native function bool Initialize();

// Export UFoxAIAction_ChooseTarget::execApplyFilter(FFrame&, void* const)
native function array<Pawn> ApplyFilter(const out array<Pawn> InEnemies, const out array<FoxTypes.ETargetFilter> InFilters);

// Export UFoxAIAction_ChooseTarget::execChooseTarget(FFrame&, void* const)
native function bool ChooseTarget(const out array<Pawn> InEnemies, out Pawn ChosenTarget);

// Export UFoxAIAction_ChooseTarget::execIsEnemyFiltered(FFrame&, void* const)
native function bool IsEnemyFiltered(Pawn InEnemy, const out array<FoxTypes.ETargetFilter> InFilters);

// Export UFoxAIAction_ChooseTarget::execChoosePreferredTarget(FFrame&, void* const)
native function int ChoosePreferredTarget(const out array<Pawn> InValidTargets);

// Export UFoxAIAction_ChooseTarget::execScoreTargetOnPreference(FFrame&, void* const)
native function float ScoreTargetOnPreference(Pawn InEnemy, const FoxTypes.ETargetPreference InPreference);

defaultproperties
{
    TargetingInfo=bOverrideTargetClosest=true,CloseRangeThreshold=512.0,bOverrideTargetAttacker=true,HowRecentlySeconds=2.0,Stickiness=10.0,Preference=ETargetPreference.ETTB_Closest,Filters=(159),
/* Exception thrown while deserializing TargetingInfo
System.ArgumentException: Der angeforderte Wert "__OnActivateItem__Delegate_855638015" konnte nicht gefunden werden.
   bei System.Enum.TryParseEnum(Type enumType, String value, Boolean ignoreCase, EnumResult& parseResult)
   bei System.Enum.Parse(Type enumType, String value, Boolean ignoreCase)
   bei UELib.Core.UDefaultProperty.Deserialize()
   bei UELib.Core.UDefaultProperty.DeserializeDefaultPropertyValue(PropertyType type, DeserializeFlags& deserializeFlags) */
    begin object name=TG class=FoxAIModifier_TimeGate
        MinTimeBetweenActivations=2.0
    object end
    // Reference: FoxAIModifier_TimeGate'Default__FoxAIAction_ChooseTarget.TG'
    Modifier=TG
    begin object name=SL class=FoxAIAction_Sleep
        Duration=0.30
    object end
    // Reference: FoxAIAction_Sleep'Default__FoxAIAction_ChooseTarget.SL'
    SubActions(0)=SL
}